#!/bin/bash
#####
##   Declare constants
#####
#Notes it does not have the check needed for samples with more than 1000 rootfiles
analysis=TTHbb_SL #HeavyNeutrino_DR #HeavyNeutrino_SigTopDY #EventCounter
series=TTHbbjpt10_
tasks=(
TTWqq3
TTZqq3
ST
SaT
DY
WJets
WW
WZ
ZZ
)
datasets=(
TTWJetsToQQ_TuneCUETP8M1_13TeV-amcatnloFXFX-madspin-pythia8
TTZToQQ_TuneCUETP8M1_13TeV-amcatnlo-pythia8
ST_tW_top_5f_inclusiveDecays_13TeV-powheg-pythia8_TuneCUETP8M1
ST_tW_antitop_5f_inclusiveDecays_13TeV-powheg-pythia8_TuneCUETP8M1
DYJetsToLL_M-50_TuneCUETP8M1_13TeV-madgraphMLM-pythia8
WJetsToLNu_TuneCUETP8M1_13TeV-madgraphMLM-pythia8
WW_TuneCUETP8M1_13TeV-pythia8
WZ_TuneCUETP8M1_13TeV-pythia8
ZZ_TuneCUETP8M1_13TeV-pythia8
)
storagepath=/eos/cms/store/cmst3/user/fromeo/TTHbbjpt10/
#####
##   Start counting the files
#####
rm numfilesqueue.txt
pos=0
for d in ${datasets[@]}; do
  dataset=${datasets[$pos]}
  task=${tasks[$pos]}
  #eos
  eos ls $storagepath/$dataset/crab_$series$task/ > temp.txt
  sed -i -- 's/'crab_$series$task'\// /g' temp.txt
  daytime=`cat temp.txt | awk '{print $NF}'`
  rm temp.txt
  eos ls $storagepath/$dataset/crab_$series$task/$daytime/ > temp.txt
  sed -i -- 's/'$daytime'\// /g' temp.txt
  subfolders=`cat temp.txt | awk '{print $NF}'`
  rm temp.txt
  for sf in $subfolders; do
    eos ls $storagepath/$dataset/crab_$series$task/$daytime/$sf >> temp.txt
  done
  eosfinishedfiles=`cat temp.txt | grep root | wc -l` 
  #event counter
  queuefinishedfiles=`ls $series$task/$analysis/*root | wc -l`
  if [ "$eosfinishedfiles" -ne "$queuefinishedfiles" ]; then
    echo "$series$task $eosfinishedfiles $queuefinishedfiles"
  fi
  echo -n "$queuefinishedfiles, " >> numfilesqueue.txt
  let pos=pos+1
done
rm temp.txt
