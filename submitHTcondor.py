#!/usr/bin/env python
# This is a simple script to show how HTcondor works

# To run it
# python3 submitHTcondor.py -t myTest

## Imports 
import os, shutil, sys, subprocess
from argparse import ArgumentParser

## User inputs
parser = ArgumentParser()
parser.add_argument('-t', '--task', dest='task', action='store', type=str, default='test')  # Name of the task (e.g. vdM2023, emittanceScanFillX, ...)
parser.add_argument('-s', '--submit', dest='submit', action='store', choices=[True,False], type=bool, default=False)
parser.add_argument('-qh', '--queueHours', dest='queueHours', action='store', type=int, default=24)
parser.add_argument('-i', '--iterations', dest='iterations', action='store', type=int, default='1') # It means you run for a maximum time of iterations*time_sleep
parser.add_argument('-ts', '--timeSleep', dest='timeSleep', action='store', type=str, default='1s')
args = parser.parse_args()
task = args.task
submit = args.submit
queue_hours = args.queueHours
iterations = args.iterations
time_sleep = args.timeSleep

## Main
def main():
    initial_path = os.popen("pwd").read().strip()  # .strip() removes empty characters
    # Create and enter the folder for the task
    create_task_folder(task)
    # Create the folders needed by HTcondor
    prepare_htcondor_folders()
    # Prepare the scripts to carry out your tasks, as if you were running locally
    current_path = os.popen("pwd").read().strip()
    for f in range(0, 3):
        prepare_sh_file(f, current_path)
    # Prepare the script to parallelize via HTcondor the execution of the .sh files
    prepare_cfg_file()
    # Run the script
    if submit:
        os.system("condor_submit %s.cfg" % task)
    # Come back to the initial directory
    os.chdir(initial_path)

## Functions
def create_task_folder(task):
    if not os.path.exists(task):
        os.makedirs(task)
    else:
        shutil.rmtree(task)
        os.makedirs(task)
    os.chdir(task)

def prepare_htcondor_folders():
    if not os.path.exists("error"):
        os.makedirs("error")  # Do it here to avoid conflicts with !(*cfg) in prepare_cfg_file
    if not os.path.exists("log"):
        os.makedirs("log")
    if not os.path.exists("output"):
        os.makedirs("output")

def prepare_sh_file(f, current_path):
    file_name = "ciao_"+str(f)
    sub_file = open(file_name + '.sh', "w")
    print("#!/bin/bash", file=sub_file)
    print("pushd %s" % current_path, file=sub_file)
    print("popd", file=sub_file)
    print("echo \"ciao %s\" >> ciao_%s_output.txt" % (f,f), file=sub_file)
    sub_file.close()

def prepare_cfg_file():
    sub_file = open(task + '.cfg', "w")
    print("universe = vanilla", file=sub_file)
    print("executable = $(filename)", file=sub_file)
    print("output = output/$Fn(filename).out", file=sub_file)
    print("error = error/$Fn(filename).err", file=sub_file)
    print("log = log/$Fn(filename).log", file=sub_file)
    #print("should_transfer_files = YES", file=sub_file)
    #print("when_to_transfer_output = ON_EXIT", file=sub_file)
    #print("transfer_output_remaps = \"$Fn(filename)_Skim.root=/eos%s%s/%s/%s/%s/$Fn(filename)_Skim.root\"" % (lnfPath, analysis, task, year, process_name), file=sub_file)
    print("+AccountingGroup = \"group_u_CMS.CAF.COMM\"", file=sub_file)
    print("+MaxRuntime = 60*60*%s" % queue_hours, file=sub_file)
    print("request_cpus = 1", file=sub_file)
    print("requirements = (OpSysAndVer =?= \"CentOS7\")", file=sub_file)
    print("queue filename matching files *.sh", file=sub_file)
    sub_file.close()

if __name__ == '__main__':
    main()
