#!/bin/bash
#####
##   Declare constants
#####
#Notes it does not have the check needed for samples with more than 1000 rootfiles
analysis=TTHbb_SL #HeavyNeutrino_DR #HeavyNeutrino_SigTopDY #EventCounter
series=TTHbbjpt10_
tasks=(
TTHbb3
TTHnbb2
TT
SEleB
SEleC
SEleD
SMuB
SMuC
SMuD2
)
datasets=(
ttHTobb_M125_13TeV_powheg_pythia8
ttHToNonbb_M125_13TeV_powheg_pythia8
TT_TuneCUETP8M1_13TeV-powheg-pythia8
SingleElectron
SingleElectron
SingleElectron
SingleMuon
SingleMuon
SingleMuon
)
storagepath=/eos/cms/store/cmst3/user/fromeo/TTHbbjpt10/
#####
##   Start counting the files
#####
rm numfilesqueue.txt
pos=0
for d in ${datasets[@]}; do
  dataset=${datasets[$pos]}
  task=${tasks[$pos]}
  #eos
  eos ls $storagepath/$dataset/crab_$series$task/ > temp.txt
  sed -i -- 's/'crab_$series$task'\// /g' temp.txt
  daytime=`cat temp.txt | awk '{print $NF}'`
  rm temp.txt
  eos ls $storagepath/$dataset/crab_$series$task/$daytime/ > temp.txt
  sed -i -- 's/'$daytime'\// /g' temp.txt
  subfolders=`cat temp.txt | awk '{print $NF}'`
  rm temp.txt
  for sf in $subfolders; do
    eos ls $storagepath/$dataset/crab_$series$task/$daytime/$sf >> temp.txt
  done
  eosfinishedfiles=`cat temp.txt | grep root | wc -l` 
  #event counter
  queuefinishedfiles=`ls $series$task/$analysis/*root | wc -l`
  if [ "$eosfinishedfiles" -ne "$queuefinishedfiles" ]; then
    echo "$series$task $eosfinishedfiles $queuefinishedfiles"
  fi
  echo -n "$queuefinishedfiles, " >> numfilesqueue.txt
  let pos=pos+1
done
rm temp.txt
