#!/bin/bash
#####
##   Declare constants
#####
#Notes it does not have the check needed for samples with more than 1000 rootfiles
#Skimming options (it assumes that a skimmer called Rootplizer_$analysis.cc is in the same folder from where you run the script)
analysis=TTHbb_SL #HeavyNeutrino_DR #HeavyNeutrino_SigTopDY #EventCounter
series=TTHbbjpt10_
tasks=(
TTHbb3
TTHnbb2
TT
SEleB
SEleC
SEleD
SMuB
SMuC
SMuD2
)
datasets=(
ttHTobb_M125_13TeV_powheg_pythia8
ttHToNonbb_M125_13TeV_powheg_pythia8
TT_TuneCUETP8M1_13TeV-powheg-pythia8
SingleElectron
SingleElectron
SingleElectron
SingleMuon
SingleMuon
SingleMuon
)
storagepath=/eos/cms/store/cmst3/user/fromeo/TTHbbjpt10/
rootstorage=root://eoscms/
queue=1nh
submitjobs=1 #1 will submit jobs, 0 is for tests
#####
##   Initialise the proxy 
#####
echo "Initialise the proxy"
voms-proxy-init --voms cms
cp /tmp/x509up_u30997 .
#####
##   Start the skimming 
#####
proxypath=`pwd`
d=0
for dat in ${datasets[@]};
do
  #Prepare it
  dataset=${datasets[$d]}
  task=${tasks[$d]}
  echo " " 
  echo "Skim "${datasets[$d]}" "${tasks[$d]}""
  echo " "
  cd $series$task 
  rm -r $analysis #Delete it: because you do not want to risk having old files in the new merging
  mkdir $analysis
  cd $analysis
  cp $proxypath/Rootplizer\_$analysis.cc .
  cp $proxypath/Rootplizer\_$analysis.h .
  export X509_USER_PROXY=$proxypath/x509up_u30997
  #Take data needed to complete full path with files
  eos ls $storagepath/$dataset/crab_$series$task/ > temp.txt
  sed -i -- 's/'crab_$series$task'\// /g' temp.txt
  daytime=`cat temp.txt | awk '{print $NF}'`
  rm temp.txt
  #Take all the sub-folders
  eos ls $storagepath/$dataset/crab_$series$task/$daytime/ > temp.txt
  sed -i -- 's/'$daytime'\// /g' temp.txt
  subfolders=`cat temp.txt | awk '{print $NF}'`
  rm temp.txt
  pos=1
  for sf in $subfolders
  do
    #Take the root files
    eos ls $storagepath/$dataset/crab_$series$task/$daytime/$sf >> temp.txt
    #1000 should be the max num of files per subfolder
    list=`cat temp.txt | sed -e "s/.*\///" | grep root | awk '{ print $NF }'`
    for f in $list
    do
      #Prepare the macro
      outputfile=`pwd`
      sed -e 's#Rootplizer_'$analysis'#Rootplizer_'$analysis'\_'$pos'#g' Rootplizer\_$analysis\.cc > Rootplizer\_$analysis\_$pos\.cc
      cp Rootplizer\_$analysis\.h Rootplizer\_$analysis\_$pos\.h
      #Prepare the script for the queue
      echo "#!/bin/bash" >> queue\_$pos\.sh
      echo "cd "$proxypath"" >> queue\_$pos\.sh
      echo "eval \`scramv1 runtime -sh\`" >> queue\_$pos\.sh
      echo "export X509_USER_PROXY="$outputfile"/x509up_u30997" >> queue\_$pos\.sh
      echo "cd "$outputfile"" >> queue\_$pos\.sh
      echo "root -b -q Rootplizer_"$analysis\_$pos".cc+'(\""$rootstorage$storagepath$dataset\/crab_$series$task\/$daytime\/$sf\/$f"\",\""$outputfile"/"$f"\")'" >> queue\_$pos\.sh
      #Submit the job
      if [ "$submitjobs" == 1 ]
      then
        bsub -q $queue -J $task\_$pos < queue\_$pos\.sh
      fi
      let pos=pos+1
    done
    rm temp.txt
  done
  cd $proxypath
  let d=d+1
done
#Other info
#root -l root://xrootd-cms.infn.it//store/user/fromeoeavyCompositeMajoranaNeutrino_L3000_M1500_eejj_CalcHep/crab_NewSig_eejj_L3000_M1500/161031_164904/0000/OutTree_1.root
#root -l root://eoscms//eos/cms/store/cmst3/user/fromeo/eejjL9000M500.root
#root -l root://eoscms//eos/cms/store/user/fromeo/HeavyNeutrino/eejjL9000M500.root
      #sed -e 's#Rootplizer_'$analysis'#Rootplizer_'$analysis'\_'$pos'#g; s#INPUTFILE#'$rootstorage$dataset\/crab_$series$task\/$daytime\/$sf\/$f'#g; s#OUTPUTFILE#'$outputfile'/'$f'#g' Rootplizer\_$analysis\.cc > Rootplizer\_$analysis\_$pos\.cc
