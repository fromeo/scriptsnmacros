#!/bin/bash
list=(
TTHbb_TTHbb
TTHbb_TTHnbb
TTHbb_TT
TTHbb_ST
TTHbb_SaT
TTHbb_TTWqq
TTHbb_DY
TTHbb_WJets
TTHbb_SEleB
TTHbb_SEleC
TTHbb_SEleD
TTHbb_SEleE
TTHbb_SEleF
TTHbb_SEleG
TTHbb_SMuB
TTHbb_SMuC
TTHbb_SMuD
TTHbb_SMuE
TTHbb_SMuF
TTHbb_SMuG
)

rm tempfull.txt
pos=0
for d in ${list[@]}; 
do
  rm temp.txt
  crab status $d/crab_$d > temp.txt
  crab status $d/crab_$d >> tempfull.txt
  echo "${list[$pos]}"
  cat temp.txt | grep -A 2 "Jobs status:" | grep 'running\|failed\|finished\|idle'
  let pos=pos+1
done
  #crab status --verboseErrors $d/crab_$d
  #crab resubmit $d/crab_$d
