/**
This Macro
Prepares the rootplas to be used in the TTHbb (SL channel) analysis, reading as input the BSM3G nutplas

Need to specify
0. See Declare constants
1. In Rootplizer_TTHbb_SL.cc take care of which data samples you are considering (for the moment SMu and SEle)
*/
/////
//   To run: root -b -q -l Rootplizer_TTHbb_SL.cc+'("/afs/cern.ch/work/f/fromeo/public/TestingRootFiles/TT_1.root","OutTree_1_rootplas.root")'
/////
/////
//   Prepare Root and Roofit
/////
#include "TFile.h"
#include "TObject.h"
#include "TTree.h"
#include "TTreePlayer.h"
#include "TGraph.h"
#include <fstream>
#include <iostream>
#include <iomanip>
#include <iostream>
#include <TLorentzVector.h>
using namespace std;
////
//   Declare constants
/////
const int nentries = -1;//-1 is all entries
/////
//   Declare functions 
/////
//Selection
void Muon_sel();
pair<double,double> Muon_IDSF(double pt, double eta);
pair<double,double> Muon_IsoSF(double pt, double eta);
pair<double,double> Muon_TrkSF(double eta);
void Electron_sel();
pair<double,double> Electron_ID(double pt, double eta);
pair<double,double> Electron_GsfSF(double eta);
void Jet_sel();
bool jet_isLoose(double neutralHadEnergyFraction, double neutralEmEnergyFraction, double chargedMultiplicity, double numberOfConstituents, double chargedHadEnergyFraction, double chargedEmEnergyFraction);
void BJetness_getval();
//Weights
double CalculatePileupWeight(int trueInteractions);
double get_wgtlumi(string FileName);
//Utils
double deltaPhi(double phi1, double phi2);
double deltaEta(double eta1, double eta2);
double deltaR(double dphi, double deta);
//Variable handleling
void rSetBranchAddress(TTree* readingtree, string sample);
void wSetBranchAddress(TTree* newtree, string sample);
void wClearInitialization(string sample);
void rGetEntry(Long64_t tentry, string sample);
/////
//   Variables to be read
/////
//Trigger
int rHLT_IsoMu22; TBranch* b_rHLT_IsoMu22 = 0;
int rHLT_IsoTkMu22; TBranch* b_rHLT_IsoTkMu22 = 0;
int rHLT_Ele27_eta2p1_WPTight_Gsf; TBranch* b_rHLT_Ele27_eta2p1_WPTight_Gsf = 0;
//Muon 
vector<double>* rMuon_pt; TBranch* b_rMuon_pt = 0;
vector<double>* rMuon_eta; TBranch* b_rMuon_eta = 0;
vector<double>* rMuon_phi; TBranch* b_rMuon_phi = 0;
vector<double>* rMuon_energy; TBranch* b_rMuon_energy = 0;
vector<double>* rMuon_tight; TBranch* b_rMuon_tight = 0;
vector<double>* rMuon_relIsoDeltaBetaR04; TBranch* b_rMuon_relIsoDeltaBetaR04 = 0;
//Electron
vector<double>* rpatElectron_pt; TBranch* b_rpatElectron_pt = 0;
vector<double>* rpatElectron_eta; TBranch* b_rpatElectron_eta = 0;
vector<double>* rpatElectron_phi; TBranch* b_rpatElectron_phi = 0;
vector<double>* rpatElectron_energy; TBranch* b_rpatElectron_energy = 0;
vector<double>* rpatElectron_inCrack; TBranch* b_rpatElectron_inCrack = 0;
vector<double>* rpatElectron_isPassMvanontrig; TBranch* b_rpatElectron_isPassMvanontrig = 0;
vector<double>* rpatElectron_relIsoRhoEA; TBranch* b_rpatElectron_relIsoRhoEA = 0;
vector<double>* rpatElectron_SCeta; TBranch* b_rpatElectron_SCeta = 0;
vector<double>* rpatElectron_full5x5_sigmaIetaIeta; TBranch* b_rpatElectron_full5x5_sigmaIetaIeta = 0;
vector<double>* rpatElectron_hOverE; TBranch* b_rpatElectron_hOverE = 0;
vector<double>* rpatElectron_ecalPFClusterIso; TBranch* b_rpatElectron_ecalPFClusterIso = 0;
vector<double>* rpatElectron_hcalPFClusterIso; TBranch* b_rpatElectron_hcalPFClusterIso = 0;
vector<double>* rpatElectron_isolPtTracks; TBranch* b_rpatElectron_isolPtTracks = 0;
vector<double>* rpatElectron_dEtaIn; TBranch* b_rpatElectron_dEtaIn = 0;
vector<double>* rpatElectron_dPhiIn; TBranch* b_rpatElectron_dPhiIn = 0;
//Jet
vector<double>* rJet_pt; TBranch* b_rJet_pt = 0;
vector<double>* rJet_eta; TBranch* b_rJet_eta = 0;
vector<double>* rJet_phi; TBranch* b_rJet_phi = 0;
vector<double>* rJet_energy; TBranch* b_rJet_energy = 0;
vector<double>* rJet_Uncorr_pt; TBranch* b_rJet_Uncorr_pt = 0;
vector<double>* rJet_JesSF; TBranch* b_rJet_JesSF = 0;
vector<double>* rJet_JerSF; TBranch* b_rJet_JerSF = 0;
vector<double>* rJet_neutralHadEnergyFraction; TBranch* b_rJet_neutralHadEnergyFraction = 0;
vector<double>* rJet_neutralEmEnergyFraction; TBranch* b_rJet_neutralEmEnergyFraction = 0;
vector<double>* rJet_chargedMultiplicity; TBranch* b_rJet_chargedMultiplicity = 0;
vector<double>* rJet_numberOfConstituents; TBranch* b_rJet_numberOfConstituents = 0;
vector<double>* rJet_chargedHadronEnergyFraction; TBranch* b_rJet_chargedHadronEnergyFraction = 0;
vector<double>* rJet_chargedEmEnergyFraction; TBranch* b_rJet_chargedEmEnergyFraction = 0;
vector<double>* rJet_newpfCombinedInclusiveSecondaryVertexV2BJetTags; TBranch* b_rJet_newpfCombinedInclusiveSecondaryVertexV2BJetTags = 0;
//BJetness
int rBJetness_isSingleLepton; TBranch* b_rBJetness_isSingleLepton = 0;
int rBJetness_isDoubleLepton; TBranch* b_rBJetness_isDoubleLepton = 0;
vector<double>* rBJetness_numjet; TBranch* b_rBJetness_numjet = 0;
vector<double>* rBJetness_jetschpvass; TBranch* b_rBJetness_jetschpvass = 0;
vector<double>* rBJetness_jetschfrompv; TBranch* b_rBJetness_jetschfrompv = 0;
vector<double>* rBJetness_jetschip3dval; TBranch* b_rBJetness_jetschip3dval = 0;
vector<double>* rBJetness_jetschip3dsig; TBranch* b_rBJetness_jetschip3dsig = 0;
vector<double>* rBJetness_jetschip2dval; TBranch* b_rBJetness_jetschip2dval = 0;
vector<double>* rBJetness_jetschip2dsig; TBranch* b_rBJetness_jetschip2dsig = 0;
vector<double>* rBJetness_jetschisgoodtrk; TBranch* b_rBJetness_jetschisgoodtrk = 0;
vector<double>* rBJetness_jetschtrkpur; TBranch* b_rBJetness_jetschtrkpur = 0;
vector<double>* rBJetness_jetschpt; TBranch* b_rBJetness_jetschpt = 0;
vector<double>* rBJetness_jetschen; TBranch* b_rBJetness_jetschen = 0;
vector<double>* rBJetness_num_pdgid_eles; TBranch* b_rBJetness_num_pdgid_eles = 0;
vector<double>* rBJetness_num_soft_eles; TBranch* b_rBJetness_num_soft_eles = 0;
vector<double>* rBJetness_num_vetonoipnoiso_eles; TBranch* b_rBJetness_num_vetonoipnoiso_eles = 0;
vector<double>* rBJetness_num_loosenoipnoiso_eles; TBranch* b_rBJetness_num_loosenoipnoiso_eles = 0;
vector<double>* rBJetness_num_veto_eles; TBranch* b_rBJetness_num_veto_eles = 0;
vector<double>* rBJetness_num_loose_eles; TBranch* b_rBJetness_num_loose_eles = 0;
vector<double>* rBJetness_num_medium_eles; TBranch* b_rBJetness_num_medium_eles = 0;
vector<double>* rBJetness_num_tight_eles; TBranch* b_rBJetness_num_tight_eles = 0;
vector<double>* rBJetness_num_mvatrig_eles; TBranch* b_rBJetness_num_mvatrig_eles = 0;
vector<double>* rBJetness_num_mvanontrig_eles; TBranch* b_rBJetness_num_mvanontrig_eles = 0;
vector<double>* rBJetness_num_mvatrigwp90_eles; TBranch* b_rBJetness_num_mvatrigwp90_eles = 0;
vector<double>* rBJetness_num_mvanontrigwp90_eles; TBranch* b_rBJetness_num_mvanontrigwp90_eles = 0;
vector<double>* rBJetness_num_heep_eles; TBranch* b_rBJetness_num_heep_eles = 0;
vector<double>* rBJetness_num_pdgid_mus; TBranch* b_rBJetness_num_pdgid_mus = 0;
vector<double>* rBJetness_num_loose_mus; TBranch* b_rBJetness_num_loose_mus = 0;
vector<double>* rBJetness_num_soft_mus; TBranch* b_rBJetness_num_soft_mus = 0;
vector<double>* rBJetness_num_medium_mus; TBranch* b_rBJetness_num_medium_mus = 0;
vector<double>* rBJetness_num_tight_mus; TBranch* b_rBJetness_num_tight_mus = 0;
vector<double>* rBJetness_num_highpt_mus; TBranch* b_rBJetness_num_highpt_mus = 0;
vector<double>* rBJetness_num_POGisGood_mus; TBranch* b_rBJetness_num_POGisGood_mus = 0;
vector<double>* rBJetness_numjettrks; TBranch* b_rBJetness_numjettrks = 0;
vector<double>* rBJetness_numjettrkspv; TBranch* b_rBJetness_numjettrkspv = 0;
vector<double>* rBJetness_numjettrksnopv; TBranch* b_rBJetness_numjettrksnopv = 0;
vector<double>* rBJetness_npvTrkOVcollTrk; TBranch* b_rBJetness_npvTrkOVcollTrk = 0;
vector<double>* rBJetness_pvTrkOVcollTrk; TBranch* b_rBJetness_pvTrkOVcollTrk = 0;
vector<double>* rBJetness_npvTrkOVpvTrk; TBranch* b_rBJetness_npvTrkOVpvTrk = 0;
vector<double>* rBJetness_npvPtOVcollPt; TBranch* b_rBJetness_npvPtOVcollPt = 0;
vector<double>* rBJetness_pvPtOVcollPt; TBranch* b_rBJetness_pvPtOVcollPt = 0;
vector<double>* rBJetness_npvPtOVpvPt; TBranch* b_rBJetness_npvPtOVpvPt = 0;
vector<double>* rBJetness_avnum2v; TBranch* b_rBJetness_avnum2v = 0;
vector<double>* rBJetness_avnumno2v; TBranch* b_rBJetness_avnumno2v = 0;
vector<double>* rBJetness_avdca3d2t; TBranch* b_rBJetness_avdca3d2t = 0;
vector<double>* rBJetness_avdca3dno2t; TBranch* b_rBJetness_avdca3dno2t = 0;
vector<double>* rBJetness_avdca3d; TBranch* b_rBJetness_avdca3d = 0;
vector<double>* rBJetness_avdca2d2t; TBranch* b_rBJetness_avdca2d2t = 0;
vector<double>* rBJetness_avdca2dno2t; TBranch* b_rBJetness_avdca2dno2t = 0;
vector<double>* rBJetness_avdca2d; TBranch* b_rBJetness_avdca2d = 0;
vector<double>* rBJetness_chi2; TBranch* b_rBJetness_chi2 = 0;
vector<double>* rBJetness_avprel; TBranch* b_rBJetness_avprel = 0;
vector<double>* rBJetness_avppar; TBranch* b_rBJetness_avppar = 0;
vector<double>* rBJetness_avetarel; TBranch* b_rBJetness_avetarel = 0;
vector<double>* rBJetness_avetapar; TBranch* b_rBJetness_avetapar = 0;
vector<double>* rBJetness_avdr; TBranch* b_rBJetness_avdr = 0;
vector<double>* rBJetness_avpreljetpt; TBranch* b_rBJetness_avpreljetpt = 0;
vector<double>* rBJetness_avpreljeten; TBranch* b_rBJetness_avpreljeten = 0;
vector<double>* rBJetness_avpparjetpt; TBranch* b_rBJetness_avpparjetpt = 0;
vector<double>* rBJetness_avpparjeten; TBranch* b_rBJetness_avpparjeten = 0;
vector<double>* rBJetness_avip3d_val; TBranch* b_rBJetness_avip3d_val = 0;
vector<double>* rBJetness_avip3d_sig; TBranch* b_rBJetness_avip3d_sig = 0;
vector<double>* rBJetness_avsip3d_val; TBranch* b_rBJetness_avsip3d_val = 0;
vector<double>* rBJetness_avsip3d_sig; TBranch* b_rBJetness_avsip3d_sig = 0;
vector<double>* rBJetness_numip3dpos; TBranch* b_rBJetness_numip3dpos = 0;
vector<double>* rBJetness_numip3dneg; TBranch* b_rBJetness_numip3dneg = 0;
vector<double>* rBJetness_avip2d_val; TBranch* b_rBJetness_avip2d_val = 0;
vector<double>* rBJetness_avip2d_sig; TBranch* b_rBJetness_avip2d_sig = 0;
vector<double>* rBJetness_avsip2d_val; TBranch* b_rBJetness_avsip2d_val = 0;
vector<double>* rBJetness_avsip2d_sig; TBranch* b_rBJetness_avsip2d_sig = 0;
vector<double>* rBJetness_numip2dpos; TBranch* b_rBJetness_numip2dpos = 0;
vector<double>* rBJetness_numip2dneg; TBranch* b_rBJetness_numip2dneg = 0;
vector<double>* rBJetness_avip1d_val; TBranch* b_rBJetness_avip1d_val = 0;
vector<double>* rBJetness_avip1d_sig; TBranch* b_rBJetness_avip1d_sig = 0;
vector<double>* rBJetness_avsip1d_val; TBranch* b_rBJetness_avsip1d_val = 0;
vector<double>* rBJetness_avsip1d_sig; TBranch* b_rBJetness_avsip1d_sig = 0;
vector<double>* rBJetnessFV_num_leps; TBranch* b_rBJetnessFV_num_leps = 0;
vector<double>* rBJetnessFV_npvTrkOVcollTrk; TBranch* b_rBJetnessFV_npvTrkOVcollTrk = 0;
vector<double>* rBJetnessFV_avip3d_val; TBranch* b_rBJetnessFV_avip3d_val = 0;
vector<double>* rBJetnessFV_avip3d_sig; TBranch* b_rBJetnessFV_avip3d_sig = 0;
vector<double>* rBJetnessFV_avsip3d_sig; TBranch* b_rBJetnessFV_avsip3d_sig = 0;
vector<double>* rBJetnessFV_avip1d_sig; TBranch* b_rBJetnessFV_avip1d_sig = 0;
//Weights
double rbWeight; TBranch* b_rbWeight = 0;
int rnBestVtx; TBranch* b_rnBestVtx = 0;
double rPUWeight; TBranch* b_rPUWeight = 0;
double rEVENT_genWeight; TBranch* b_rEVENT_genWeight = 0;
double rtrueInteractions; TBranch* b_rtrueInteractions = 0;
//Evt 
int rttHFCategory; TBranch* b_rttHFCategory = 0;
/////
//   Variables to be saved
/////
//
//From variables to be read
//
//Trigger
//Muon 
vector<double>* Muon_pt = new std::vector<double>;
vector<double>* Muon_eta = new std::vector<double>;
vector<double>* Muon_phi = new std::vector<double>;
vector<double>* Muon_energy = new std::vector<double>;
vector<double>* Muon_tight = new std::vector<double>;
vector<double>* Muon_relIsoDeltaBetaR04 = new std::vector<double>;
vector<double>* Muon_IDSFval = new std::vector<double>;
vector<double>* Muon_IsoSFval = new std::vector<double>;
vector<double>* Muon_TrkSFval = new std::vector<double>;
vector<double>* Muon_IDSFerr = new std::vector<double>;
vector<double>* Muon_IsoSFerr = new std::vector<double>;
vector<double>* Muon_TrkSFerr = new std::vector<double>;
//Electron
vector<double>* patElectron_pt = new std::vector<double>;
vector<double>* patElectron_eta = new std::vector<double>;
vector<double>* patElectron_phi = new std::vector<double>;
vector<double>* patElectron_energy = new std::vector<double>;
vector<double>* patElectron_inCrack = new std::vector<double>;
vector<double>* patElectron_isPassMvanontrig = new std::vector<double>;
vector<double>* patElectron_relIsoRhoEA = new std::vector<double>;
vector<double>* patElectron_SCeta = new std::vector<double>;
vector<double>* patElectron_full5x5_sigmaIetaIeta = new std::vector<double>;
vector<double>* patElectron_hOverE = new std::vector<double>;
vector<double>* patElectron_ecalPFClusterIso = new std::vector<double>;
vector<double>* patElectron_hcalPFClusterIso = new std::vector<double>;
vector<double>* patElectron_isolPtTracks = new std::vector<double>;
vector<double>* patElectron_dEtaIn = new std::vector<double>;
vector<double>* patElectron_dPhiIn = new std::vector<double>;
vector<double>* patElectron_IDSFval = new std::vector<double>;
vector<double>* patElectron_GsfSFval = new std::vector<double>;
vector<double>* patElectron_IDSFerr = new std::vector<double>;
vector<double>* patElectron_GsfSFerr = new std::vector<double>;
//Jet
vector<double>* Jet_pt = new std::vector<double>;
vector<double>* Jet_eta = new std::vector<double>;
vector<double>* Jet_phi = new std::vector<double>;
vector<double>* Jet_energy = new std::vector<double>;
vector<double>* Jet_Uncorr_pt = new std::vector<double>;
vector<double>* Jet_JesSF = new std::vector<double>;
vector<double>* Jet_JerSF = new std::vector<double>;
vector<double>* Jet_neutralHadEnergyFraction = new std::vector<double>;
vector<double>* Jet_neutralEmEnergyFraction = new std::vector<double>;
vector<double>* Jet_chargedMultiplicity = new std::vector<double>;
vector<double>* Jet_numberOfConstituents = new std::vector<double>;
vector<double>* Jet_chargedHadronEnergyFraction = new std::vector<double>;
vector<double>* Jet_chargedEmEnergyFraction = new std::vector<double>;
vector<double>* Jet_newpfCombinedInclusiveSecondaryVertexV2BJetTags = new std::vector<double>;
//BJetness
int BJetness_isSingleLepton;
int BJetness_isDoubleLepton;
double BJetness_numjet;
vector<double>* BJetness_jetschpvass = new std::vector<double>;
vector<double>* BJetness_jetschfrompv = new std::vector<double>;
vector<double>* BJetness_jetschip3dval = new std::vector<double>;
vector<double>* BJetness_jetschip3dsig = new std::vector<double>;
vector<double>* BJetness_jetschip2dval = new std::vector<double>;
vector<double>* BJetness_jetschip2dsig = new std::vector<double>;
vector<double>* BJetness_jetschisgoodtrk = new std::vector<double>;
vector<double>* BJetness_jetschtrkpur = new std::vector<double>;
vector<double>* BJetness_jetschpt = new std::vector<double>;
vector<double>* BJetness_jetschen = new std::vector<double>;
double BJetness_num_pdgid_eles;
double BJetness_num_soft_eles;
double BJetness_num_vetonoipnoiso_eles;
double BJetness_num_loosenoipnoiso_eles;
double BJetness_num_veto_eles;
double BJetness_num_loose_eles;
double BJetness_num_medium_eles;
double BJetness_num_tight_eles;
double BJetness_num_mvatrig_eles;
double BJetness_num_mvanontrig_eles;
double BJetness_num_mvatrigwp90_eles;
double BJetness_num_mvanontrigwp90_eles;
double BJetness_num_heep_eles;
double BJetness_num_pdgid_mus;
double BJetness_num_loose_mus;
double BJetness_num_soft_mus;
double BJetness_num_medium_mus;
double BJetness_num_tight_mus;
double BJetness_num_highpt_mus;
double BJetness_num_POGisGood_mus;
double BJetness_numjettrks;
double BJetness_numjettrkspv;
double BJetness_numjettrksnopv;
double BJetness_npvTrkOVcollTrk;
double BJetness_pvTrkOVcollTrk;
double BJetness_npvTrkOVpvTrk;
double BJetness_npvPtOVcollPt;
double BJetness_pvPtOVcollPt;
double BJetness_npvPtOVpvPt;
double BJetness_avnum2v;
double BJetness_avnumno2v;
double BJetness_avdca3d2t;
double BJetness_avdca3dno2t;
double BJetness_avdca3d;
double BJetness_avdca2d2t;
double BJetness_avdca2dno2t;
double BJetness_avdca2d;
double BJetness_chi2;
double BJetness_avprel;
double BJetness_avppar;
double BJetness_avetarel;
double BJetness_avetapar;
double BJetness_avdr;
double BJetness_avpreljetpt;
double BJetness_avpreljeten;
double BJetness_avpparjetpt;
double BJetness_avpparjeten;
double BJetness_avip3d_val;
double BJetness_avip3d_sig;
double BJetness_avsip3d_val;
double BJetness_avsip3d_sig;
double BJetness_numip3dpos;
double BJetness_numip3dneg;
double BJetness_avip2d_val;
double BJetness_avip2d_sig;
double BJetness_avsip2d_val;
double BJetness_avsip2d_sig;
double BJetness_numip2dpos;
double BJetness_numip2dneg;
double BJetness_avip1d_val;
double BJetness_avip1d_sig;
double BJetness_avsip1d_val;
double BJetness_avsip1d_sig;
double BJetnessFV_num_leps;
double BJetnessFV_npvTrkOVcollTrk;
double BJetnessFV_avip3d_val;
double BJetnessFV_avip3d_sig;
double BJetnessFV_avsip3d_sig;
double BJetnessFV_avip1d_sig;
//Weights
double bWeight;
double nBestVtx;
double PUWeight;
double EVENT_genWeight;
double lumi_wgt;
double lepsf;
//Evt
double ttHFCategory;
//
//New variables
//
//Evt
double isSingleMuonEvt;
double isSingleElectronEvt;
double Jet_num;
double Jet_numbLoose;
double Jet_numbMedium;
double Jet_numbTight;
/////
//   Variables to be used in the main (and not saved)
/////
