/**
This Macro   
1. Plots variables of signal and background overlaid

Need to specify
1. See Declare Constants
*/
/////
//   To run: root -l SigBkgVarsOverlaid_new.cc+
/////
/////
//   Prepare Root and Roofit
/////
#include "TFile.h"
#include "TH1F.h"
#include "THStack.h"
#include "TLegend.h"
#include "TTree.h"
#include "TTreePlayer.h"
#include "TStyle.h"
#include "TGaxis.h"
#include "TCanvas.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TEfficiency.h"
#include <fstream>
#include <iostream>
#include <iomanip>
#include <iostream>
using namespace std;
/////
//   Declare constants
/////
const string path     = "/afs/cern.ch/work/f/fromeo/CMSSW_8_0_20_FW/src/BSMFramework/BSM3G_TNT_Maker/Anflow/";
const char *samples[] = {
//"TTHnobb_CAT_0","TTHnobb_CAT_1","TTHnobb_CAT_2","TTHnobb_CAT_3","TTHnobb_CAT_4","TTHnobb_CAT_5",
//"TTW_CAT_0","TTW_CAT_1","TTW_CAT_2","TTW_CAT_3","TTW_CAT_4","TTW_CAT_5"
//"TTH","TT",
"TTW"
//"TTH_RecoFull","TTW_RecoFull",
//"TTH_RecoNoMet","TTW_RecoNoMet"
//"TTH_FullV0","TTW_FullV0",
};
const string selection  = "ttHwgt";
const string suffix  = "RecoOpt";
//const string suffix  = "GenCut03V0";
//const string suffix  = "RecoNoMet";
//const string suffix  = "FullV0";
double cut_dij = 0.6;
double cut_fj = 0.2;
double cut_sj = -0.5;
const char *varfirst[]  = {"minRegH_mass"};
const char *varsecond[] = {"EVENT_genWeight"};
const char *varthird[] = {"tagJetPair_BDT"};
const char *varfourth[] = {"tagJetPair_j0bdt"};
const char *varfifth[] = {"tagJetPair_j1bdt"};
const char *vartitle[]  = {"CAT"};
const int numvar = 1;
const double inRange[numvar]    = {0};
const double endRange[numvar]   = {300};
const int    bin[numvar]        = {30};
//const double endRange[numvar]   = {5};
//const int    bin[numvar]        = {5};
bool saveplots = true;
double threshold = -1.;
//Variables
/////
//   Declare functions 
/////
TFile* Call_TFile(string rootpla);
void setTDRStyle();
/////
//   Main function
/////
void Overlaid_double(){
 //Preliminarly
 setTDRStyle();
 //Loop over variables
 vector<string> varfirsts(varfirst, varfirst + sizeof(varfirst)/sizeof(varfirst[0])); 
 vector<string> varseconds(varsecond, varsecond + sizeof(varsecond)/sizeof(varsecond[0]));
 vector<string> varthirds(varthird, varthird + sizeof(varthird)/sizeof(varthird[0]));
 vector<string> varfourths(varfourth, varfourth + sizeof(varfourth)/sizeof(varfourth[0]));
 vector<string> varfifths(varfifth, varfifth + sizeof(varfifth)/sizeof(varfifth[0]));
 vector<string> vartitles(vartitle, vartitle + sizeof(vartitle)/sizeof(vartitle[0])); 
 const uint variables_size = varfirsts.size();
 for(uint vars=0; vars<variables_size; vars++){
 //for(uint vars=0; vars<4; vars++){
  //TCanvas* c1 = new TCanvas(varseconds[vars].c_str(),varseconds[vars].c_str(),200,200,900,700);
  TCanvas* c1 = new TCanvas(varfirsts[vars].c_str(),varfirsts[vars].c_str(),200,200,800,600);
  //c1->SetLogy();
  TLegend *leg = new TLegend(0.7, 0.7, 0.9, 0.9);
  leg->SetHeader("");
  leg->SetBorderSize(0);
  //leg->SetTextSize(0.05);
  leg->SetTextSize(0.035);
  //Do plots
  //Loop over samples
  vector<string> rootplas(samples, samples + sizeof(samples)/sizeof(samples[0]));
  const uint rootplas_size = rootplas.size();
  for(uint smp=0; smp<rootplas_size; smp++){
  //for(uint smp=0; smp<3; smp++){
   //Declare histo
   uint n = smp;
   TH1F *hist = new TH1F(rootplas[smp].c_str(),rootplas[smp].c_str(),bin[vars],inRange[vars],endRange[vars]); 
   hist->SetTitle("Cut03");
   hist->SetMarkerStyle(1);
   hist->GetXaxis()->SetTitle(suffix.c_str());
   hist->GetYaxis()->SetTitle("Percentage");
   hist->SetMinimum(0);
   hist->SetMaximum(30);
   if(smp==0){
    hist->SetMarkerColor(1);
    hist->SetLineColor(1);
    //hist->SetLineStyle(4);
   }else if(smp==4){
    hist->SetMarkerColor(28);
    hist->SetLineColor(28);
   }else if(smp==6){
    hist->SetMarkerColor(9);
    hist->SetLineColor(9);
   }else if(smp==7){
    hist->SetMarkerColor(41);
    hist->SetLineColor(41);
    hist->SetLineStyle(4);
   }else if(smp==9){
    hist->SetMarkerColor(46);
    hist->SetLineColor(46);
    hist->SetLineStyle(4);
   }else if(smp==n){
    hist->SetMarkerColor(n+1);
    hist->SetLineColor(n+1);
   }
   //Make plot
   TFile* f = Call_TFile((rootplas[smp]).c_str()); TTree* tree; f->GetObject("BOOM",tree);
   //Float_t Hreg_mass = -999;
   //tree->SetBranchAddress("Hreg_mass",&Hreg_mass); 
   Float_t curr_var0;
   double curr_var1;
   vector<double> * curr_var2 = 0;
   vector<double> * curr_var3 = 0;
   vector<double> * curr_var4 = 0;
   tree->SetBranchAddress(varfirsts[vars].c_str(),&curr_var0);
   tree->SetBranchAddress(varseconds[vars].c_str(),&curr_var1);
   tree->SetBranchAddress(varthirds[vars].c_str(),&curr_var2);
   tree->SetBranchAddress(varfourths[vars].c_str(),&curr_var3);
   tree->SetBranchAddress(varfifths[vars].c_str(),&curr_var4);
   if(smp==0||smp==2||smp==4||smp==6)threshold=0.3;
   if(smp==1||smp==3||smp==5||smp==7)threshold=0.3;
   int entree = tree->GetEntries();
   int pos =0;
   int minus =0;
   for(int en=0; en<entree; en++){
    tree->GetEntry(en);
    double cat =-1;
    double minlepmass=0;
    double maxlepmass =0;
    double goodmass =0;
    double badmass =0;
    if(smp>5) cat = smp-6;
    else cat = smp;
    if(curr_var0<inRange[vars]) minlepmass =inRange[vars];
    else if(curr_var0>endRange[vars]) minlepmass = endRange[vars]-10;
    else minlepmass = curr_var0;
    double w = curr_var1/fabs(curr_var1);
    if((curr_var2->at(0)>cut_dij)&& curr_var3->at(0)>cut_fj && curr_var4->at(0)>cut_sj){//&&fabs(curr_var0-125)<25){
     hist->Fill(minlepmass,w);
     //hist->Fill(minlepmass);
     if(smp==2)cout<<w<<endl;
     if(w>=0)pos++;
     else minus ++;
     //hist->Fill(goodmass);
     //hist->Fill(0);
    }
   }
   


   cout<<"Entries of "<<rootplas[smp]<<" is "<<hist->Integral()<< " pos "<<pos <<" minus "<<minus<<endl;
   double scale = 100./hist->Integral();
   //double scale = 100./entree;
   hist->Scale(scale);
   hist->SetMaximum(50);
   hist->SetMinimum(0);
   
   if(varfirsts[vars]=="Hreg_mass"){
    double MPV = 0;
    int binmax = 0;
    hist->GetXaxis()->SetRange(2,100);
    binmax = hist->GetMaximumBin();
    MPV = hist->GetXaxis()->GetBinCenter(binmax);
    cout << "MPV= "<<MPV<<endl;
    hist->GetXaxis()->SetRange(1,100);
   }
   //Draw plot
   gStyle->SetOptStat("");     
   gStyle->SetStatColor(kWhite);
   gStyle->SetStatX(0.9); //Starting position on X axis
   gStyle->SetStatW(0.15); //Horizontal size 
   if(smp==0){
    gStyle->SetStatTextColor(1);
    gStyle->SetStatY(0.9); //Starting position on Y axis
    gStyle->SetStatX(0.6); //Starting position on Y axis
    gStyle->SetStatFontSize(0.05); //Vertical Size
   }else if(smp==4){
    gStyle->SetStatTextColor(28);
    gStyle->SetStatY(0.5); //Starting position on Y axis
    gStyle->SetStatX(0.9); //Starting position on Y axis
    gStyle->SetStatFontSize(0.05); //Vertical Size
   }else if(smp==6){
    gStyle->SetStatTextColor(9);
    gStyle->SetStatX(0.9); //Starting position on Y axis
    gStyle->SetStatY(0.4); //Starting position on Y axis
    gStyle->SetStatFontSize(0.05); //Vertical Size
   }else if(smp==7){
    gStyle->SetStatTextColor(41);
    gStyle->SetStatX(0.9); //Starting position on Y axis
    gStyle->SetStatY(0.3); //Starting position on Y axis
    gStyle->SetStatFontSize(0.05); //Vertical Size
   }else if(smp==n){
    gStyle->SetStatTextColor(n+1);
    gStyle->SetStatX(0.6); //Starting position on Y axis
    gStyle->SetStatY(0.9-0.1*n); //Starting position on Y axis
    gStyle->SetStatFontSize(0.05); //Vertical Size
   }
   if(smp==0){
    leg->AddEntry(hist,rootplas[smp].c_str(),"L"); 
    hist->Draw(""); 
   }else if(smp==1){
    leg->AddEntry(hist,rootplas[smp].c_str(),"L"); 
    hist->Draw("sames");
   }else if(smp==2){
    leg->AddEntry(hist,rootplas[smp].c_str(),"L"); 
    hist->Draw("sames");
   }else if(smp==n){
    leg->AddEntry(hist,rootplas[smp].c_str(),"L"); 
    hist->Draw("sames");
   }
  } 
  leg->Draw();
  //string namefile = suffix+"_2_Dec_Cut06_02_m05_regmass.png";
  //string namefile = suffix+"_2_Dec_Cut"+to_string(cut_dij)+"_"+to_string(cut_fj)+"_"+to_string(cut_sj)+"_minlepmass.png";
  string namefile = suffix+"_2_Dec_Cut"+to_string(cut_dij)+"_"+to_string(cut_fj)+"_"+to_string(cut_sj)+"_goodlepmass.png";
  //string namefile = suffix+"_2_Dec_Cut"+to_string(cut_dij)+"_"+to_string(cut_fj)+"_"+to_string(cut_sj)+"_classification.png";
  if(saveplots) c1->SaveAs(namefile.c_str());
 }
}
/////
//   Call TFile to be read
/////
TFile* Call_TFile(string rootpla){
 string dotroot   = ".root";
 //string file_name = path+selection+"_"+rootpla+"_TTHnobb"+dotroot;
 string file_name = path+selection+"_"+rootpla+"_"+suffix+dotroot;
 cout << file_name << endl;
 TFile* f = new TFile(file_name.c_str(),"update");
 return f;
}
/////
//   Set setTDRStyle_modified (from link https://twiki.cern.ch/twiki/pub/CMS/TRK10001/setTDRStyle_modified.C)
/////
void setTDRStyle(){
  TStyle *tdrStyle = new TStyle("tdrStyle","Style for P-TDR");

  // For the canvas:
  tdrStyle->SetCanvasBorderMode(0);
  tdrStyle->SetCanvasColor(kWhite);
  tdrStyle->SetCanvasDefH(600); //Height of canvas
  tdrStyle->SetCanvasDefW(600); //Width of canvas
  tdrStyle->SetCanvasDefX(0);   //POsition on screen
  tdrStyle->SetCanvasDefY(0);
  // For the Pad:
  tdrStyle->SetPadBorderMode(0);
  // tdrStyle->SetPadBorderSize(Width_t size = 1);
  tdrStyle->SetPadColor(kWhite);
  tdrStyle->SetPadGridX(false);
  tdrStyle->SetPadGridY(false);
  tdrStyle->SetGridColor(0);
  tdrStyle->SetGridStyle(3);
  tdrStyle->SetGridWidth(1);
  // For the frame:
  tdrStyle->SetFrameBorderMode(0);
  tdrStyle->SetFrameBorderSize(1);
  tdrStyle->SetFrameFillColor(0);
  tdrStyle->SetFrameFillStyle(0);
  tdrStyle->SetFrameLineColor(1);
  tdrStyle->SetFrameLineStyle(1);
  tdrStyle->SetFrameLineWidth(1);

  // For the histo:
  tdrStyle->SetHistFillColor(0);
  // tdrStyle->SetHistFillStyle(0);
  tdrStyle->SetHistLineColor(1);
  tdrStyle->SetHistLineStyle(0);
  tdrStyle->SetHistLineWidth(1);
  // tdrStyle->SetLegoInnerR(Float_t rad = 0.5);
  // tdrStyle->SetNumberContours(Int_t number = 20);
//  tdrStyle->SetEndErrorSize(0);
  tdrStyle->SetErrorX(0.);
//  tdrStyle->SetErrorMarker(20);

  tdrStyle->SetMarkerStyle(20);

  //For the fit/function:
  tdrStyle->SetOptFit(0);
  tdrStyle->SetFitFormat("5.4g");
  //tdrStyle->SetFuncColor(1);
  tdrStyle->SetFuncStyle(1);
  tdrStyle->SetFuncWidth(1);

  //For the date:
  tdrStyle->SetOptDate(0);
  // tdrStyle->SetDateX(Float_t x = 0.01);
  // tdrStyle->SetDateY(Float_t y = 0.01);

  // For the statistics box:
  tdrStyle->SetOptFile(0);
  //tdrStyle->SetOptStat("mr"); // To display the mean and RMS:   SetOptStat("mr");
  //tdrStyle->SetStatColor(kWhite);
  //tdrStyle->SetStatColor(kGray);
  //tdrStyle->SetStatFont(42);

  //tdrStyle->SetTextSize(11);
  tdrStyle->SetTextAlign(11);

  //tdrStyle->SetStatTextColor(1);
  //tdrStyle->SetStatFormat("6.4g");
  tdrStyle->SetStatBorderSize(0);
  //tdrStyle->SetStatX(1.); //Starting position on X axis
  //tdrStyle->SetStatY(1.); //Starting position on Y axis
  //tdrStyle->SetStatFontSize(0.025); //Vertical Size
  //tdrStyle->SetStatW(0.25); //Horizontal size 
  // tdrStyle->SetStatStyle(Style_t style = 1001);

  // Margins:
  tdrStyle->SetPadTopMargin(0.05);
  tdrStyle->SetPadBottomMargin(0.165);
  tdrStyle->SetPadLeftMargin(0.1);
  tdrStyle->SetPadRightMargin(0.05);

  // For the Global title:
  //  tdrStyle->SetOptTitle(0);
  tdrStyle->SetTitleFont(42);
  tdrStyle->SetTitleColor(1);
  tdrStyle->SetTitleTextColor(1);
  tdrStyle->SetTitleFillColor(10);
  tdrStyle->SetTitleFontSize(0.5);
  tdrStyle->SetTitleH(0.05); // Set the height of the title box
  //tdrStyle->SetTitleW(0); // Set the width of the title box
  tdrStyle->SetTitleX(0.15); // Set the position of the title box
  tdrStyle->SetTitleY(1.0); // Set the position of the title box
  // tdrStyle->SetTitleStyle(Style_t style = 1001);
  tdrStyle->SetTitleBorderSize(0);

  // For the axis titles:
  tdrStyle->SetTitleColor(1, "XYZ");
  tdrStyle->SetTitleFont(42, "XYZ");
  tdrStyle->SetTitleSize(0.045, "X");
  tdrStyle->SetTitleSize(0.06, "YZ");
  // tdrStyle->SetTitleXSize(Float_t size = 0.02); // Another way to set the size?
  // tdrStyle->SetTitleYSize(Float_t size = 0.02);
  tdrStyle->SetTitleXOffset(1.5);
  //tdrStyle->SetTitleYOffset(1.0);
  tdrStyle->SetTitleOffset(0.75, "Y"); // Another way to set the Offset

  // For the axis labels:

  tdrStyle->SetLabelColor(1, "XYZ");
  tdrStyle->SetLabelFont(42, "XYZ");
  tdrStyle->SetLabelOffset(0.007, "XYZ");
  tdrStyle->SetLabelSize(0.05, "XYZ");

  // For the axis:

  tdrStyle->SetAxisColor(1, "XYZ");
  tdrStyle->SetStripDecimals(kTRUE);
  tdrStyle->SetTickLength(0.03, "XYZ");
  tdrStyle->SetNdivisions(510, "XYZ");
  tdrStyle->SetPadTickX(1);  // To get tick marks on the opposite side of the frame
  tdrStyle->SetPadTickY(1);

  // Change for log plots:
  tdrStyle->SetOptLogx(0);
  tdrStyle->SetOptLogy(0);
  tdrStyle->SetOptLogz(0);
  // Postscript options:
  // tdrStyle->SetPaperSize(15.,15.);
  // tdrStyle->SetLineScalePS(Float_t scale = 3);
  // tdrStyle->SetLineStyleString(Int_t i, const char* text);
  // tdrStyle->SetHeaderPS(const char* header);
  // tdrStyle->SetTitlePS(const char* pstitle);
  // tdrStyle->SetBarOffset(Float_t baroff = 0.5);
  // tdrStyle->SetBarWidth(Float_t barwidth = 0.5);
  // tdrStyle->SetPaintTextFormat(const char* format = "g");
  // tdrStyle->SetPalette(Int_t ncolors = 0, Int_t* colors = 0);
  // tdrStyle->SetTimeOffset(Double_t toffset);
  // tdrStyle->SetHistMinimumZero(kTRUE);

  tdrStyle->cd();
}
