#!/bin/bash
#New Heavy Neutrino Sampels
datasets=(
/ttHTobb_M125_13TeV_powheg_pythia8/RunIISpring16MiniAODv2-PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14-v1/MINIAODSIM
/TT_TuneCUETP8M1_13TeV-powheg-pythia8/RunIISpring16MiniAODv2-PUSpring16RAWAODSIM_reHLT_80X_mcRun2_asymptotic_v14_ext3-v1/MINIAODSIM
/ST_tW_top_5f_inclusiveDecays_13TeV-powheg-pythia8_TuneCUETP8M1/RunIISpring16MiniAODv2-PUSpring16_80X_mcRun2_asymptotic_2016_miniAODv2_v0-v2/MINIAODSIM
/ST_tW_antitop_5f_inclusiveDecays_13TeV-powheg-pythia8_TuneCUETP8M1/RunIISpring16MiniAODv2-PUSpring16_80X_mcRun2_asymptotic_2016_miniAODv2_v0-v1/MINIAODSIM
/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-madgraphMLM-pythia8/RunIISpring16MiniAODv2-PUSpring16_80X_mcRun2_asymptotic_2016_miniAODv2_v0_ext1-v1/MINIAODSIM
/WJetsToLNu_TuneCUETP8M1_13TeV-madgraphMLM-pythia8/RunIISpring16MiniAODv2-PUSpring16_80X_mcRun2_asymptotic_2016_miniAODv2_v0-v1/MINIAODSIM
/WW_TuneCUETP8M1_13TeV-pythia8/RunIISpring16MiniAODv2-PUSpring16_80X_mcRun2_asymptotic_2016_miniAODv2_v0-v1/MINIAODSIM
/WZ_TuneCUETP8M1_13TeV-pythia8/RunIISpring16MiniAODv2-PUSpring16_80X_mcRun2_asymptotic_2016_miniAODv2_v0-v1/MINIAODSIM
/ZZ_TuneCUETP8M1_13TeV-pythia8/RunIISpring16MiniAODv2-PUSpring16_80X_mcRun2_asymptotic_2016_miniAODv2_v0-v1/MINIAODSIM
/SingleElectron/Run2016B-PromptReco-v2/MINIAOD
/SingleElectron/Run2016C-PromptReco-v2/MINIAOD
/SingleElectron/Run2016D-PromptReco-v2/MINIAOD
/SingleMuon/Run2016B-PromptReco-v2/MINIAOD
/SingleMuon/Run2016C-PromptReco-v2/MINIAOD
/SingleMuon/Run2016D-PromptReco-v2/MINIAOD
)
tasks=(
_TTHbb
2_TT
_ST
_SaT
_DY
_WJets
_WW
_WZ
_ZZ
_SEleB
_SEleC
_SEleD
_SMuB
2_SMuC
_SMuD
)
#Search datasets
d=0
for dat in ${datasets[@]};
do
  #./das_client.py --query="dataset=/${datasets[$d]}$campaign | grep dataset.name"
  #./das_client.py --query="file dataset=/${datasets[$d]}$campaign" > temp.txt
  #info=`cat temp.txt | wc -l`
  ./das_client.py --query="file dataset=${datasets[$d]} | sum(file.nevents)" > temp.txt
  info=`cat temp.txt | grep "sum(file.nevents)" | awk '{print substr ($1, 19,20)}'`
  echo "${tasks[$d]} $info"
  let d=d+1
done
