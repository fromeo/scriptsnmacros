/**
This Macro
1. Calls trees, fix cuts and creates ntuples with cut data  

Need to specify
0. See Declare constants
1. const double numfiles[rootplasize]
*/
/////
//   To run: root -l SkimNtuple.cc+  
/////
/////
//   Prepare Root and Roofit
/////
#include "TFile.h"
#include "TTree.h"
#include <iostream>
using namespace std;
////
//   Declare constants
/////
//Path - samples 
const string path     = "/afs/cern.ch/work/f/fromeo/CMSSW_8_0_17_FW/src/BSMFramework/BSM3G_TNT_Maker/crab/TTHbb/EventCounter/";
const char *samples[] = {
"TTHbb",
"TTHnbb",
"TT",
"ST",
"SaT",
"DY",
"WJets",
"WW",
"WZ",
"ZZ",
}; 
const string dotroot   = ".root"; 
/////
//   Declare functions 
/////
TFile* Call_TFile(string rootpla);
/////
//   Main function
/////
void EventCounter(){
 vector<string> rootplas(samples, samples + sizeof(samples)/sizeof(samples[0]));
 const double numfiles[100] = {85, 85, 513, 7, 7, 296, 166, 6, 6, 7};
 for(uint i=0; i<rootplas.size(); i++){
  TFile* f = Call_TFile(rootplas[i]); TTree* tree; f->GetObject("evtree",tree);
  cout<<rootplas[i]<<" "<<tree->GetEntries()-numfiles[i]<<endl;
 }
}
/////
//   Call TFile to be read
/////
TFile* Call_TFile(string rootpla){
 string file_name = path+rootpla+"_EventCounter"+dotroot;
 TFile* f = new TFile(file_name.c_str(),"update");
 return f;
}
