#include "Rootplizer_TTHbb_SL.h"
/////
//   Main function
/////
//Notes
//See whether requiring the trigger or not
//Carefull with the lumi_wgt weights (check xSec and numevt)
//Assicurati anche di verificare se stai usando sample inclusivi o no (perche' il nome in get_wgtlumi cambia)
void Rootplizer_TTHbb_SL(const char * Input = "", const char * Output =""){
 //Call input file
 TFile *inputfile = TFile::Open(Input);
 TTree *readingtree = new TTree("readingtree","readingtree"); readingtree = (TTree*) inputfile->Get("TNT/BOOM");
 string sample = "MC";
 if((string(Output).find("SMu") != std::string::npos) || (string(Output).find("SEle") != std::string::npos)) sample = "data";
 rSetBranchAddress(readingtree,sample);
 //Define output file
 TFile *newfile = new TFile(Output,"recreate");
 TTree* newtree = new TTree("BOOM","BOOM");
 newtree->SetMaxTreeSize(99000000000);
 wSetBranchAddress(newtree,sample);
 //Fill new branches
 int nen = nentries; if(nentries==-1) nen = readingtree->GetEntries();
 for(Int_t en=0; en<nen; en++){
  //Ini
  wClearInitialization(sample);
  Long64_t tentry = readingtree->LoadTree(en); 
  rGetEntry(tentry,sample);
  //Muon
  Muon_sel();
  //if((rHLT_IsoMu22==1 || rHLT_IsoTkMu22==1) &&
  if(Muon_pt->size()==1 && Muon_pt->at(0)>25 && fabs(Muon_eta->at(0))<2.1 && Muon_relIsoDeltaBetaR04->at(0)<0.15) isSingleMuonEvt = 1;
  else isSingleMuonEvt = 0;
  //Electron
  Electron_sel();
  //if(rHLT_Ele27_eta2p1_WPTight_Gsf==1 &&
  if(patElectron_pt->size()==1 && patElectron_pt->at(0)>30 && fabs(patElectron_eta->at(0))<2.1) isSingleElectronEvt = 1;
  else isSingleElectronEvt = 0;
  //Jet
  Jet_sel();
  //Evt selection
  if(!(Jet_num>=4 && Jet_numbMedium>=2 && isSingleMuonEvt+isSingleElectronEvt==1)) continue; 
  //Access quantities
  BJetness_getval();
  //Weights
  nBestVtx = rnBestVtx;
  bWeight  = rbWeight;
  if(sample!="data"){
   lumi_wgt = get_wgtlumi(Output);
   PUWeight = rPUWeight; //CalculatePileupWeight(rtrueInteractions);
   EVENT_genWeight = rEVENT_genWeight/fabs(rEVENT_genWeight); //CalculatePileupWeight(rtrueInteractions);
   if(isSingleMuonEvt)     lepsf = Muon_IDSFval->at(0)*Muon_IsoSFval->at(0)*Muon_TrkSFval->at(0);
   if(isSingleElectronEvt) lepsf = patElectron_IDSFval->at(0)*patElectron_GsfSFval->at(0);
  }
  //Evt
  ttHFCategory = rttHFCategory;
  //Fill tree
  newtree->Fill();
 }
 //Save new file
 newfile->cd();
 newfile->Write();
 newfile->Close();
} 
/////
//   Analysis functions
/////
//Selection
//Muon
void Muon_sel(){
 for(uint mu_en = 0; mu_en<rMuon_pt->size(); mu_en++){
  if(!(rMuon_pt->at(mu_en)>15 && fabs(rMuon_eta->at(mu_en))<2.4
       && rMuon_tight->at(mu_en)==1 && rMuon_relIsoDeltaBetaR04->at(mu_en)<0.25
      )) continue;
  Muon_pt->push_back(rMuon_pt->at(mu_en)); 
  Muon_eta->push_back(rMuon_eta->at(mu_en));
  Muon_phi->push_back(rMuon_phi->at(mu_en));
  Muon_energy->push_back(rMuon_energy->at(mu_en));
  Muon_tight->push_back(rMuon_tight->at(mu_en));
  Muon_relIsoDeltaBetaR04->push_back(rMuon_relIsoDeltaBetaR04->at(mu_en));
  Muon_IDSFval->push_back(Muon_IDSF(rMuon_pt->at(mu_en),rMuon_eta->at(mu_en)).first);
  Muon_IsoSFval->push_back(Muon_IsoSF(rMuon_pt->at(mu_en),rMuon_eta->at(mu_en)).first);
  Muon_TrkSFval->push_back(Muon_TrkSF(rMuon_eta->at(mu_en)).first);
  Muon_IDSFerr->push_back(Muon_IDSF(rMuon_pt->at(mu_en),rMuon_eta->at(mu_en)).second);
  Muon_IsoSFerr->push_back(Muon_IsoSF(rMuon_pt->at(mu_en),rMuon_eta->at(mu_en)).second);
  Muon_TrkSFerr->push_back(Muon_TrkSF(rMuon_eta->at(mu_en)).second);
 }
}
pair<double,double> Muon_IDSF(double pt, double eta){
 double sfval = 1;
 double sferr = 1;
 if(pt<=25)//20<pt && pt<=25)
 {
  if(0<fabs(eta) && fabs(eta)<=0.9){sfval = 0.958376; sferr = 0.00256685;}
  if(0.9<fabs(eta) && fabs(eta)<=1.2){sfval = 0.969183; sferr = 0.00375759;}
  if(1.2<fabs(eta) && fabs(eta)<=2.1){sfval = 0.991418; sferr = 0.00197292;}
  if(2.1<fabs(eta) && fabs(eta)<=2.4){sfval = 0.985294; sferr = 0.00368382;}
 }
 if(25<pt && pt<=30){
  if(0<fabs(eta) && fabs(eta)<=0.9){sfval = 0.970763; sferr = 0.00126508;}
  if(0.9<fabs(eta) && fabs(eta)<=1.2){sfval = 0.966866; sferr = 0.00206616;}
  if(1.2<fabs(eta) && fabs(eta)<=2.1){sfval = 0.989645; sferr = 0.00109714;}
  if(2.1<fabs(eta) && fabs(eta)<=2.4){sfval = 0.975853; sferr = 0.00230061;}
 }
 if(30<pt && pt<=40){
  if(0<fabs(eta) && fabs(eta)<=0.9){sfval = 0.975881; sferr = 0.000481227;}
  if(0.9<fabs(eta) && fabs(eta)<=1.2){sfval = 0.971573; sferr = 0.000830174;}
  if(1.2<fabs(eta) && fabs(eta)<=2.1){sfval = 0.990198; sferr = 0.000450168;}
  if(2.1<fabs(eta) && fabs(eta)<=2.4){sfval = 0.973918; sferr = 0.00113783;}
 }
 if(40<pt && pt<=50){
  if(0<fabs(eta) && fabs(eta)<=0.9){sfval = 0.976182; sferr = 0.000387079;}
  if(0.9<fabs(eta) && fabs(eta)<=1.2){sfval = 0.970849; sferr = 0.000627464;}
  if(1.2<fabs(eta) && fabs(eta)<=2.1){sfval = 0.990848; sferr = 0.000200121;}
  if(2.1<fabs(eta) && fabs(eta)<=2.4){sfval = 0.969894; sferr = 0.00108924;}
 }
 if(50<pt && pt<=60){
  if(0<fabs(eta) && fabs(eta)<=0.9){sfval = 0.972266; sferr = 0.000999986;}
  if(0.9<fabs(eta) && fabs(eta)<=1.2){sfval = 0.971497; sferr = 0.00167629;}
  if(1.2<fabs(eta) && fabs(eta)<=2.1){sfval = 0.991589; sferr = 0.00115025;}
  if(2.1<fabs(eta) && fabs(eta)<=2.4){sfval = 0.981064; sferr = 0.00354674;}
 }
 if(60<pt && pt<=100){
  if(0<fabs(eta) && fabs(eta)<=0.9){sfval = 0.973523; sferr = 0.00201625;}
  if(0.9<fabs(eta) && fabs(eta)<=1.2){sfval = 0.968216; sferr = 0.00347208;}
  if(1.2<fabs(eta) && fabs(eta)<=2.1){sfval = 0.989786; sferr = 0.00262609;}
  if(2.1<fabs(eta) && fabs(eta)<=2.4){sfval = 0.975218; sferr = 0.00763513;}
 }
 if(pt>100)//100<pt && pt<=200)
 {
  if(0<fabs(eta) && fabs(eta)<=0.9){sfval = 0.98764; sferr = 0.0160819;}
  if(0.9<fabs(eta) && fabs(eta)<=1.2){sfval = 1.02714; sferr = 0.0224793;}
  if(1.2<fabs(eta) && fabs(eta)<=2.1){sfval = 1.01349; sferr = 0.00944602;}
  if(2.1<fabs(eta) && fabs(eta)<=2.4){sfval = 0.91808; sferr = 0.0796353;}
 }
 return make_pair(sfval,sferr);
}
pair<double,double> Muon_IsoSF(double pt, double eta){
 double sfval = 1;
 double sferr = 1;
 if(pt<=25)//20<pt && pt<=25)
 {
  if(0<fabs(eta) && fabs(eta)<=0.9){sfval = 0.982323; sferr = 0.00298554;}
  if(0.9<fabs(eta) && fabs(eta)<=1.2){sfval = 0.986009; sferr = 0.00456872;}
  if(1.2<fabs(eta) && fabs(eta)<=2.1){sfval = 0.986859; sferr = 0.00220422;}
  if(2.1<fabs(eta) && fabs(eta)<=2.4){sfval = 0.983397; sferr = 0.00381719;}
 }
 if(25<pt && pt<=30){
  if(0<fabs(eta) && fabs(eta)<=0.9){sfval = 0.988605; sferr = 0.00166344;}
  if(0.9<fabs(eta) && fabs(eta)<=1.2){sfval = 0.994709; sferr = 0.00289202;}
  if(1.2<fabs(eta) && fabs(eta)<=2.1){sfval = 0.993867; sferr = 0.00140074;}
  if(2.1<fabs(eta) && fabs(eta)<=2.4){sfval = 0.987529; sferr = 0.00237643;}
 }
 if(30<pt && pt<=40){
  if(0<fabs(eta) && fabs(eta)<=0.9){sfval = 0.992043; sferr = 0.000580556;}
  if(0.9<fabs(eta) && fabs(eta)<=1.2){sfval = 0.99809; sferr = 0.00106105;}
  if(1.2<fabs(eta) && fabs(eta)<=2.1){sfval = 0.997524; sferr = 0.000582381;}
  if(2.1<fabs(eta) && fabs(eta)<=2.4){sfval = 0.993539; sferr = 0.00101822;}
 }
 if(40<pt && pt<=50){
  if(0<fabs(eta) && fabs(eta)<=0.9){sfval = 0.994218; sferr = 0.000327784;}
  if(0.9<fabs(eta) && fabs(eta)<=1.2){sfval = 0.997873; sferr = 0.000448171;}
  if(1.2<fabs(eta) && fabs(eta)<=2.1){sfval = 0.99795; sferr = 0.000246853;}
  if(2.1<fabs(eta) && fabs(eta)<=2.4){sfval = 0.996392; sferr = 0.000394046;}
 }
 if(50<pt && pt<=60){
  if(0<fabs(eta) && fabs(eta)<=0.9){sfval = 0.996457; sferr = 0.000556209;}
  if(0.9<fabs(eta) && fabs(eta)<=1.2){sfval = 0.999352; sferr = 0.000926904;}
  if(1.2<fabs(eta) && fabs(eta)<=2.1){sfval = 0.999048; sferr = 0.000520563;}
  if(2.1<fabs(eta) && fabs(eta)<=2.4){sfval = 0.997946; sferr = 0.00108252;}
 }
 if(60<pt && pt<=100){
  if(0<fabs(eta) && fabs(eta)<=0.9){sfval = 0.999023; sferr = 0.000703851;}
  if(0.9<fabs(eta) && fabs(eta)<=1.2){sfval = 0.999509; sferr = 0.00110414;}
  if(1.2<fabs(eta) && fabs(eta)<=2.1){sfval = 0.998433; sferr = 0.000669639;}
  if(2.1<fabs(eta) && fabs(eta)<=2.4){sfval = 1.00073; sferr = 0.00153644;}
 }
 if(pt>100)//100<pt && pt<=200)
 {
  if(0<fabs(eta) && fabs(eta)<=0.9){sfval = 1.00007; sferr = 0.00222242;}
  if(0.9<fabs(eta) && fabs(eta)<=1.2){sfval = 0.995709; sferr = 0.00424566;}
  if(1.2<fabs(eta) && fabs(eta)<=2.1){sfval = 1.00571; sferr = 0.00281406;}
  if(2.1<fabs(eta) && fabs(eta)<=2.4){sfval = 0.99858; sferr = 0.00587265;}
 }
 return make_pair(sfval,sferr);
}
pair<double,double> Muon_TrkSF(double eta){
 double sfval = 1;
 double sferr = 1;
 const double x[10] = {-2.23090284685015616e+00, -1.82699807131713809e+00, -1.34607008919971971e+00, -8.43046106233286685e-01, -2.97940760993056608e-01, 2.98252730786497722e-01, 8.43136487245492505e-01, 1.34752802994490373e+00, 1.82701299844882636e+00, 2.23329880623762422e+00};
 const double y[10] = {9.82399009186853522e-01, 9.91746789037933008e-01, 9.95944961092376846e-01, 9.93413142541369476e-01, 9.91460688530866996e-01, 9.94680143661991423e-01, 9.96666389348924819e-01, 9.94933892427240618e-01, 9.91186607207322878e-01, 9.76811919457875155e-01};
 TGraph* tg = new TGraph(10,x,y); 
 sfval = tg->Eval(eta);
 return make_pair(sfval,sferr);
}
//Electron
void Electron_sel(){
 for(uint ele_en = 0; ele_en<rpatElectron_pt->size(); ele_en++){
  if(!(rpatElectron_pt->at(ele_en)>15 && fabs(rpatElectron_eta->at(ele_en))<2.4 && rpatElectron_inCrack->at(ele_en)==0 && rpatElectron_isPassMvanontrig->at(ele_en)==1 && rpatElectron_relIsoRhoEA->at(ele_en)<0.15 
       //&& ((fabs(rpatElectron_SCeta->at(ele_en))<1.4442 && rpatElectron_full5x5_sigmaIetaIeta->at(ele_en)<0.012 && rpatElectron_hOverE->at(ele_en)<0.09 && (rpatElectron_ecalPFClusterIso->at(ele_en)/rpatElectron_pt->at(ele_en))<0.37 && (rpatElectron_hcalPFClusterIso->at(ele_en)/rpatElectron_pt->at(ele_en))<0.25 && (rpatElectron_isolPtTracks->at(ele_en)/rpatElectron_pt->at(ele_en))<0.18 && fabs(rpatElectron_dEtaIn->at(ele_en))<0.0095 && fabs(rpatElectron_dPhiIn->at(ele_en))<0.065)
       //|| (fabs(rpatElectron_SCeta->at(ele_en))>1.5660 && rpatElectron_full5x5_sigmaIetaIeta->at(ele_en)<0.033 && rpatElectron_hOverE->at(ele_en)<0.09 && (rpatElectron_ecalPFClusterIso->at(ele_en)/rpatElectron_pt->at(ele_en))<0.45 && (rpatElectron_hcalPFClusterIso->at(ele_en)/rpatElectron_pt->at(ele_en))<0.28 && (rpatElectron_isolPtTracks->at(ele_en)/rpatElectron_pt->at(ele_en))<0.18))
      )) continue;
  //bool ismatched = false;
  //for(uint mu_en = 0; mu_en<Muon_pt->size(); mu_en++) if(deltaR(deltaPhi(Muon_phi->at(mu_en),rpatElectron_phi->at(ele_en)),deltaEta(Muon_eta->at(mu_en),rpatElectron_eta->at(ele_en)))<0.05) {ismatched = true; break;}
  //if(ismatched) continue;
  patElectron_pt->push_back(rpatElectron_pt->at(ele_en));
  patElectron_eta->push_back(rpatElectron_eta->at(ele_en));
  patElectron_phi->push_back(rpatElectron_phi->at(ele_en));
  patElectron_energy->push_back(rpatElectron_energy->at(ele_en));
  patElectron_inCrack->push_back(rpatElectron_inCrack->at(ele_en));
  patElectron_isPassMvanontrig->push_back(rpatElectron_isPassMvanontrig->at(ele_en));
  patElectron_relIsoRhoEA->push_back(rpatElectron_relIsoRhoEA->at(ele_en));
  patElectron_SCeta->push_back(rpatElectron_SCeta->at(ele_en));
  patElectron_full5x5_sigmaIetaIeta->push_back(rpatElectron_full5x5_sigmaIetaIeta->at(ele_en));
  patElectron_hOverE->push_back(rpatElectron_hOverE->at(ele_en));
  patElectron_ecalPFClusterIso->push_back(rpatElectron_ecalPFClusterIso->at(ele_en));
  patElectron_hcalPFClusterIso->push_back(rpatElectron_hcalPFClusterIso->at(ele_en));
  patElectron_isolPtTracks->push_back(rpatElectron_isolPtTracks->at(ele_en));
  patElectron_dEtaIn->push_back(rpatElectron_dEtaIn->at(ele_en));
  patElectron_dPhiIn->push_back(rpatElectron_dPhiIn->at(ele_en));
  patElectron_IDSFval->push_back(Electron_ID(rpatElectron_pt->at(ele_en),rpatElectron_eta->at(ele_en)).first);
  patElectron_GsfSFval->push_back(Electron_GsfSF(rpatElectron_eta->at(ele_en)).first);
  patElectron_IDSFerr->push_back(Electron_ID(rpatElectron_pt->at(ele_en),rpatElectron_eta->at(ele_en)).second);
  patElectron_GsfSFerr->push_back(Electron_GsfSF(rpatElectron_eta->at(ele_en)).second);
 }
}
pair<double,double> Electron_ID(double pt, double eta){
 double sfval = 1;
 double sferr = 1;
 if(pt<=20){//10<pt && pt<=20)
  if(-2.5<eta && eta<=-2){sfval = 0.83737; sferr = 0.0237874;}
  if(-2<eta && eta<=-1.566){sfval = 0.799714; sferr = 0.0141407;}
  if(-1.566<eta && eta<=-1.444){sfval = 0.951583; sferr = 0.0776466;}
  if(-1.444<eta && eta<=-0.8){sfval = 0.914992; sferr = 0.0352903;}
  if(-0.8<eta && eta<=0){sfval = 0.90313; sferr = 0.0272266;}
  if(0<eta && eta<=0.8){sfval = 0.927622; sferr = 0.0268911;}
  if(0.8<eta && eta<=1.444){sfval = 0.937206; sferr = 0.0351763;}
  if(1.444<eta && eta<=1.566){sfval = 0.968401; sferr = 0.0774164;}
  if(1.566<eta && eta<=2){sfval = 0.842647; sferr = 0.0138921;}
  if(2<eta && eta<=2.5){sfval = 0.850575; sferr = 0.0239472;}
 }
 if(20<pt && pt<=30){
  if(-2.5<eta && eta<=-2){sfval = 0.862958; sferr = 0.0132085;}
  if(-2<eta && eta<=-1.566){sfval = 0.871007; sferr = 0.0132405;}
  if(-1.566<eta && eta<=-1.444){sfval = 0.890744; sferr = 0.0517097;}
  if(-1.444<eta && eta<=-0.8){sfval = 0.923476; sferr = 0.0209732;}
  if(-0.8<eta && eta<=0){sfval = 0.9199; sferr = 0.0168032;}
  if(0<eta && eta<=0.8){sfval = 0.944931; sferr = 0.0168032;}
  if(0.8<eta && eta<=1.444){sfval = 0.919166; sferr = 0.0209278;}
  if(1.444<eta && eta<=1.566){sfval = 0.865356; sferr = 0.0515486;}
  if(1.566<eta && eta<=2){sfval = 0.866417; sferr = 0.0131643;}
  if(2<eta && eta<=2.5){sfval = 0.891856; sferr = 0.0132896;}
 }
 if(30<pt && pt<=40){
  if(-2.5<eta && eta<=-2){sfval = 0.91492; sferr = 0.00831223;}
  if(-2<eta && eta<=-1.566){sfval = 0.916188; sferr = 0.00481392;}
  if(-1.566<eta && eta<=-1.444){sfval = 0.920253; sferr = 0.0121675;}
  if(-1.444<eta && eta<=-0.8){sfval = 0.94881; sferr = 0.00803681;}
  if(-0.8<eta && eta<=0){sfval = 0.940904; sferr = 0.00442778;}
  if(0<eta && eta<=0.8){sfval = 0.961494; sferr = 0.00442778;}
  if(0.8<eta && eta<=1.444){sfval = 0.944844; sferr = 0.00803681;}
  if(1.444<eta && eta<=1.566){sfval = 0.918471; sferr = 0.0120133;}
  if(1.566<eta && eta<=2){sfval = 0.919262; sferr = 0.00481392;}
  if(2<eta && eta<=2.5){sfval = 0.927007; sferr = 0.00837403;}
 }
 if(40<pt && pt<=50){
  if(-2.5<eta && eta<=-2){sfval = 0.929825; sferr = 0.00558629;}
  if(-2<eta && eta<=-1.566){sfval = 0.938957; sferr = 0.0032966;}
  if(-1.566<eta && eta<=-1.444){sfval = 0.953162; sferr = 0.00758102;}
  if(-1.444<eta && eta<=-0.8){sfval = 0.95553; sferr = 0.00501427;}
  if(-0.8<eta && eta<=0){sfval = 0.950728; sferr = 0.0044236;}
  if(0<eta && eta<=0.8){sfval = 0.965051; sferr = 0.0044236;}
  if(0.8<eta && eta<=1.444){sfval = 0.954181; sferr = 0.00501427;}
  if(1.444<eta && eta<=1.566){sfval = 0.932322; sferr = 0.0074618;}
  if(1.566<eta && eta<=2){sfval = 0.943333; sferr = 0.00314095;}
  if(2<eta && eta<=2.5){sfval = 0.943155; sferr = 0.00558629;}
 }
 if(pt>50){//50<pt && pt<=200)
  if(-2.5<eta && eta<=-2){sfval = 0.936; sferr = 0.00652911;}
  if(-2<eta && eta<=-1.566){sfval = 0.949946; sferr = 0.00353559;}
  if(-1.566<eta && eta<=-1.444){sfval = 0.965438; sferr = 0.0081386;}
  if(-1.444<eta && eta<=-0.8){sfval = 0.957494; sferr = 0.00381244;}
  if(-0.8<eta && eta<=0){sfval = 0.956954; sferr = 0.00695617;}
  if(0<eta && eta<=0.8){sfval = 0.971143; sferr = 0.00695617;}
  if(0.8<eta && eta<=1.444){sfval = 0.963842; sferr = 0.00381244;}
  if(1.444<eta && eta<=1.566){sfval = 0.943936; sferr = 0.0081386;}
  if(1.566<eta && eta<=2){sfval = 0.958425; sferr = 0.00353559;}
  if(2<eta && eta<=2.5){sfval = 0.954442; sferr = 0.00619668;}
 }
 return make_pair(sfval,sferr);
}
pair<double,double> Electron_GsfSF(double eta){
 double sfval = 1;
 double sferr = 1;
 if(-2.5<eta && eta<=-2.4){sfval = 1.17034; sferr = 0.00966552;}
 if(-2.4<eta && eta<=-2.3){sfval = 1.00852; sferr = 0.0120181;}
 if(-2.3<eta && eta<=-2.2){sfval = 1.01047; sferr = 0.0085874;}
 if(-2.2<eta && eta<=-2){sfval = 1.00519; sferr = 0.00814729;}
 if(-2<eta && eta<=-1.8){sfval = 0.997932; sferr = 0.00754439;}
 if(-1.8<eta && eta<=-1.63){sfval = 0.991701; sferr = 0.00761482;}
 if(-1.63<eta && eta<=-1.566){sfval = 0.986486; sferr = 0.00699237;}
 if(-1.566<eta && eta<=-1.444){sfval = 0.961582; sferr = 0.0185147;}
 if(-1.444<eta && eta<=-1.2){sfval = 0.986667; sferr = 0.00602468;}
 if(-1.2<eta && eta<=-1){sfval = 0.977505; sferr = 0.00696244;}
 if(-1<eta && eta<=-0.6){sfval = 0.969388; sferr = 0.00597084;}
 if(-0.6<eta && eta<=-0.4){sfval = 0.966361; sferr = 0.00662906;}
 if(-0.4<eta && eta<=-0.2){sfval = 0.963303; sferr = 0.00634912;}
 if(-0.2<eta && eta<=0){sfval = 0.96; sferr = 0.00656714;}
 if(0<eta && eta<=0.2){sfval = 0.966189; sferr = 0.00656714;}
 if(0.2<eta && eta<=0.4){sfval = 0.979633; sferr = 0.00634912;}
 if(0.4<eta && eta<=0.6){sfval = 0.976578; sferr = 0.00662906;}
 if(0.6<eta && eta<=1){sfval = 0.980652; sferr = 0.00597084;}
 if(1<eta && eta<=1.2){sfval = 0.986735; sferr = 0.00696244;}
 if(1.2<eta && eta<=1.444){sfval = 0.98668; sferr = 0.00602468;}
 if(1.444<eta && eta<=1.566){sfval = 0.970721; sferr = 0.0185147;}
 if(1.566<eta && eta<=1.63){sfval = 0.989669; sferr = 0.00699237;}
 if(1.63<eta && eta<=1.8){sfval = 0.995872; sferr = 0.00783568;}
 if(1.8<eta && eta<=2){sfval = 0.989733; sferr = 0.007487;}
 if(2<eta && eta<=2.2){sfval = 0.994861; sferr = 0.00819214;}
 if(2.2<eta && eta<=2.3){sfval = 0.992769; sferr = 0.00850434;}
 if(2.3<eta && eta<=2.4){sfval = 0.966632; sferr = 0.0119341;}
 if(2.4<eta && eta<=2.5){sfval = 0.884021; sferr = 0.00953672;}
 return make_pair(sfval,sferr);
}
//Jet
void Jet_sel(){
 int jet_num        = 0;
 int jet_numbLoose  = 0;
 int jet_numbMedium = 0;
 int jet_numbTight  = 0;
 for(uint jet_en = 0; jet_en<rJet_pt->size(); jet_en++){
  double jet_pt = rJet_Uncorr_pt->at(jet_en)*rJet_JesSF->at(jet_en)*rJet_JerSF->at(jet_en);
  double jet_energy = rJet_energy->at(jet_en)*rJet_Uncorr_pt->at(jet_en)/rJet_pt->at(jet_en)*rJet_JesSF->at(jet_en)*rJet_JerSF->at(jet_en);
  TLorentzVector JetCorr(0,0,0,0); JetCorr.SetPtEtaPhiE(jet_pt, rJet_eta->at(jet_en), rJet_phi->at(jet_en), jet_energy);
  if(!(JetCorr.Pt()>30 && fabs(JetCorr.Eta())<2.4
     && jet_isLoose(rJet_neutralHadEnergyFraction->at(jet_en), rJet_neutralEmEnergyFraction->at(jet_en), rJet_chargedMultiplicity->at(jet_en), rJet_numberOfConstituents->at(jet_en), rJet_chargedHadronEnergyFraction->at(jet_en), rJet_chargedEmEnergyFraction->at(jet_en))
     )) continue;
  bool ismatched = false;
  for(uint mu_en = 0; mu_en<Muon_pt->size(); mu_en++) if(deltaR(deltaPhi(Muon_phi->at(mu_en),rJet_phi->at(jet_en)),deltaEta(Muon_eta->at(mu_en),rJet_eta->at(jet_en)))<0.4) {ismatched = true; break;}
  if(!ismatched) for(uint ele_en = 0; ele_en<patElectron_pt->size(); ele_en++) if(deltaR(deltaPhi(patElectron_phi->at(ele_en),rJet_phi->at(jet_en)),deltaEta(patElectron_eta->at(ele_en),rJet_eta->at(jet_en)))<0.4) {ismatched = true; break;}
  if(ismatched) continue;
  jet_num++;
  if(rJet_newpfCombinedInclusiveSecondaryVertexV2BJetTags->at(jet_en)>0.460) jet_numbLoose++;
  if(rJet_newpfCombinedInclusiveSecondaryVertexV2BJetTags->at(jet_en)>0.800) jet_numbMedium++;
  if(rJet_newpfCombinedInclusiveSecondaryVertexV2BJetTags->at(jet_en)>0.935) jet_numbTight++;
  Jet_pt->push_back(rJet_pt->at(jet_en));
  Jet_eta->push_back(rJet_eta->at(jet_en));
  Jet_phi->push_back(rJet_phi->at(jet_en));
  Jet_energy->push_back(rJet_energy->at(jet_en));
  Jet_Uncorr_pt->push_back(rJet_Uncorr_pt->at(jet_en));
  Jet_JesSF->push_back(rJet_JesSF->at(jet_en));
  Jet_JerSF->push_back(rJet_JerSF->at(jet_en));
  Jet_neutralHadEnergyFraction->push_back(rJet_neutralHadEnergyFraction->at(jet_en));
  Jet_neutralEmEnergyFraction->push_back(rJet_neutralEmEnergyFraction->at(jet_en));
  Jet_chargedMultiplicity->push_back(rJet_chargedMultiplicity->at(jet_en));
  Jet_numberOfConstituents->push_back(rJet_numberOfConstituents->at(jet_en));
  Jet_chargedHadronEnergyFraction->push_back(rJet_chargedHadronEnergyFraction->at(jet_en));
  Jet_chargedEmEnergyFraction->push_back(rJet_chargedEmEnergyFraction->at(jet_en));
  Jet_newpfCombinedInclusiveSecondaryVertexV2BJetTags->push_back(rJet_newpfCombinedInclusiveSecondaryVertexV2BJetTags->at(jet_en));
 }
 Jet_num        = jet_num;
 Jet_numbLoose  = jet_numbLoose;
 Jet_numbMedium = jet_numbMedium;
 Jet_numbTight  = jet_numbTight;
}
bool jet_isLoose(double neutralHadEnergyFraction, double neutralEmEnergyFraction, double chargedMultiplicity, double numberOfConstituents, double chargedHadEnergyFraction, double chargedEmEnergyFraction){
 bool isloose = false;
 if(neutralHadEnergyFraction<0.99 && neutralEmEnergyFraction<0.99 && numberOfConstituents>1 && chargedHadEnergyFraction>0 && chargedEmEnergyFraction<0.99 && chargedMultiplicity>0) isloose = true;
 return isloose;
}
void BJetness_getval(){
 BJetness_isSingleLepton = rBJetness_isSingleLepton;
 BJetness_isDoubleLepton = rBJetness_isDoubleLepton;
 BJetness_numjet = rBJetness_numjet->at(0);
 for(uint pfcand_en = 0; pfcand_en<rBJetness_jetschpvass->size(); pfcand_en++){
  BJetness_jetschpvass->push_back(rBJetness_jetschpvass->at(pfcand_en));
  BJetness_jetschfrompv->push_back(rBJetness_jetschfrompv->at(pfcand_en));
  BJetness_jetschip3dval->push_back(rBJetness_jetschip3dval->at(pfcand_en));
  BJetness_jetschip3dsig->push_back(rBJetness_jetschip3dsig->at(pfcand_en));
  BJetness_jetschip2dval->push_back(rBJetness_jetschip2dval->at(pfcand_en));
  BJetness_jetschip2dsig->push_back(rBJetness_jetschip2dsig->at(pfcand_en));
  BJetness_jetschisgoodtrk->push_back(rBJetness_jetschisgoodtrk->at(pfcand_en));
  BJetness_jetschtrkpur->push_back(rBJetness_jetschtrkpur->at(pfcand_en));
  BJetness_jetschpt->push_back(rBJetness_jetschpt->at(pfcand_en));
  BJetness_jetschen->push_back(rBJetness_jetschen->at(pfcand_en));
 }
 BJetness_num_pdgid_eles = rBJetness_num_pdgid_eles->at(0);
 BJetness_num_soft_eles = rBJetness_num_soft_eles->at(0);
 BJetness_num_vetonoipnoiso_eles = rBJetness_num_vetonoipnoiso_eles->at(0);
 BJetness_num_loosenoipnoiso_eles = rBJetness_num_loosenoipnoiso_eles->at(0);
 BJetness_num_veto_eles = rBJetness_num_veto_eles->at(0);
 BJetness_num_loose_eles = rBJetness_num_loose_eles->at(0);
 BJetness_num_medium_eles = rBJetness_num_medium_eles->at(0);
 BJetness_num_tight_eles = rBJetness_num_tight_eles->at(0);
 BJetness_num_mvatrig_eles = rBJetness_num_mvatrig_eles->at(0);
 BJetness_num_mvanontrig_eles = rBJetness_num_mvanontrig_eles->at(0);
 BJetness_num_mvatrigwp90_eles = rBJetness_num_mvatrigwp90_eles->at(0);
 BJetness_num_mvanontrigwp90_eles = rBJetness_num_mvanontrigwp90_eles->at(0);
 BJetness_num_heep_eles = rBJetness_num_heep_eles->at(0);
 BJetness_num_pdgid_mus = rBJetness_num_pdgid_mus->at(0);
 BJetness_num_loose_mus = rBJetness_num_loose_mus->at(0);
 BJetness_num_soft_mus = rBJetness_num_soft_mus->at(0);
 BJetness_num_medium_mus = rBJetness_num_medium_mus->at(0);
 BJetness_num_tight_mus = rBJetness_num_tight_mus->at(0);
 BJetness_num_highpt_mus = rBJetness_num_highpt_mus->at(0);
 BJetness_num_POGisGood_mus = rBJetness_num_POGisGood_mus->at(0);
 BJetness_numjettrks = rBJetness_numjettrks->at(0);
 BJetness_numjettrkspv = rBJetness_numjettrkspv->at(0);
 BJetness_numjettrksnopv = rBJetness_numjettrksnopv->at(0);
 BJetness_npvTrkOVcollTrk = rBJetness_npvTrkOVcollTrk->at(0);
 BJetness_pvTrkOVcollTrk = rBJetness_pvTrkOVcollTrk->at(0);
 BJetness_npvTrkOVpvTrk = rBJetness_npvTrkOVpvTrk->at(0);
 BJetness_npvPtOVcollPt = rBJetness_npvPtOVcollPt->at(0);
 BJetness_pvPtOVcollPt = rBJetness_pvPtOVcollPt->at(0);
 BJetness_npvPtOVpvPt = rBJetness_npvPtOVpvPt->at(0);
 BJetness_avnum2v = rBJetness_avnum2v->at(0);
 BJetness_avnumno2v = rBJetness_avnumno2v->at(0);
 BJetness_avdca3d2t = rBJetness_avdca3d2t->at(0);
 BJetness_avdca3dno2t = rBJetness_avdca3dno2t->at(0);
 BJetness_avdca3d = rBJetness_avdca3d->at(0);
 BJetness_avdca2d2t = rBJetness_avdca2d2t->at(0);
 BJetness_avdca2dno2t = rBJetness_avdca2dno2t->at(0);
 BJetness_avdca2d = rBJetness_avdca2d->at(0);
 BJetness_chi2 = rBJetness_chi2->at(0);
 BJetness_avprel = rBJetness_avprel->at(0);
 BJetness_avppar = rBJetness_avppar->at(0);
 BJetness_avetarel = rBJetness_avetarel->at(0);
 BJetness_avetapar = rBJetness_avetapar->at(0);
 BJetness_avdr = rBJetness_avdr->at(0);
 BJetness_avpreljetpt = rBJetness_avpreljetpt->at(0);
 BJetness_avpreljeten = rBJetness_avpreljeten->at(0);
 BJetness_avpparjetpt = rBJetness_avpparjetpt->at(0);
 BJetness_avpparjeten = rBJetness_avpparjeten->at(0);
 BJetness_avip3d_val = rBJetness_avip3d_val->at(0);
 BJetness_avip3d_sig = rBJetness_avip3d_sig->at(0);
 BJetness_avsip3d_val = rBJetness_avsip3d_val->at(0);
 BJetness_avsip3d_sig = rBJetness_avsip3d_sig->at(0);
 BJetness_numip3dpos = rBJetness_numip3dpos->at(0)/rBJetness_numjettrks->at(0);
 BJetness_numip3dneg = rBJetness_numip3dneg->at(0)/rBJetness_numjettrks->at(0);
 BJetness_avip2d_val = rBJetness_avip2d_val->at(0);
 BJetness_avip2d_sig = rBJetness_avip2d_sig->at(0);
 BJetness_avsip2d_val = rBJetness_avsip2d_val->at(0);
 BJetness_avsip2d_sig = rBJetness_avsip2d_sig->at(0);
 BJetness_numip2dpos = rBJetness_numip2dpos->at(0)/rBJetness_numjettrks->at(0);
 BJetness_numip2dneg = rBJetness_numip2dneg->at(0)/rBJetness_numjettrks->at(0);
 BJetness_avip1d_val = rBJetness_avip1d_val->at(0);
 BJetness_avip1d_sig = rBJetness_avip1d_sig->at(0);
 BJetness_avsip1d_val = rBJetness_avsip1d_val->at(0);
 BJetness_avsip1d_sig = rBJetness_avsip1d_sig->at(0);
 BJetnessFV_num_leps = rBJetnessFV_num_leps->at(0);
 BJetnessFV_npvTrkOVcollTrk = rBJetnessFV_npvTrkOVcollTrk->at(0);
 BJetnessFV_avip3d_val = rBJetnessFV_avip3d_val->at(0);
 BJetnessFV_avip3d_sig = rBJetnessFV_avip3d_sig->at(0);
 BJetnessFV_avsip3d_sig = rBJetnessFV_avsip3d_sig->at(0);
 BJetnessFV_avip1d_sig = rBJetnessFV_avip1d_sig->at(0);
}
//Weigths
double CalculatePileupWeight(int trueInteractions){
 double data[] =  {5.64297e-05,0.000269623,0.000382956,0.000552844,0.000979931,0.00231634,0.0072801,0.0281133,0.0770196,0.130953,0.169536,0.182158,0.161879,0.116249,0.067104,0.0316937,0.0130125,0.00540404,0.00265731,0.00140593,0.000649204,0.000236783,6.7861e-05,1.63211e-05,3.82396e-06,1.03356e-06,3.33506e-07,1.1967e-07,4.49699e-08,1.70463e-08,6.34643e-09,2.28001e-09,7.82427e-10,2.55149e-10,7.88717e-11,2.30861e-11,6.39564e-12,1.67664e-12,4.15902e-13,9.76172e-14,2.16793e-14,4.55562e-15,9.05813e-16,1.70419e-16,3.03394e-17,5.11017e-18,8.14821e-19,1.20894e-19,2.20432e-20,0,0};
 double mc[] = {0.000108643,0.000388957,0.000332882,0.00038397,0.000549167,0.00105412,0.00459007,0.0210314,0.0573688,0.103986,0.142369,0.157729,0.147685,0.121027,0.08855,0.0582866,0.0348526,0.019457,0.0107907,0.00654313,0.00463195,0.00370927,0.0031137,0.00261141,0.00215499,0.00174491,0.00138268,0.00106731,0.000798828,0.00057785,0.00040336,0.00027161,0.000176535,0.00011092,6.75502e-05,4.00323e-05,2.32123e-05,1.32585e-05,7.51611e-06,4.25902e-06,2.42513e-06,1.39077e-06,8.02452e-07,4.64159e-07,2.67845e-07,1.5344e-07,8.68966e-08,4.84931e-08,2.6606e-08,1.433e-08};
 double puw;
 puw=data[trueInteractions]/mc[trueInteractions];
 return puw;
}
double get_wgtlumi(string FileName){
 double wgt=1;
 if(FileName.find("TT") != std::string::npos) wgt=831.76/9.27087e+07;//It must be here, or will get wrong weights for other samples
 if(FileName.find("TTHbb") != std::string::npos) wgt=(0.5824*0.5071)/3.44435e+06; //(0.577*0.5071)/3772012;
 if(FileName.find("TTHnbb") != std::string::npos) wgt=(0.4176*0.5071)/3945824; //(0.423*0.5071)/3945824;
 if(FileName.find("DY") != std::string::npos) wgt=6025.2/4.85275e+07;
 //if(FileName.find("DYHT0To100") != std::string::npos) wgt=6025.2/9004328;
 //if(FileName.find("DYHT100To200") != std::string::npos) wgt=(147.40*1.23)/2605822;
 //if(FileName.find("DYHT200To400") != std::string::npos) wgt=(40.99*1.23)/962195;
 //if(FileName.find("DYHT400To600") != std::string::npos) wgt=(5.678*1.23)/1069003;
 //if(FileName.find("DYHT600ToInf") != std::string::npos) wgt=(2.198*1.23)/1031103;
 if(FileName.find("ST") != std::string::npos) wgt=35.6/998399;
 if(FileName.find("SaT") != std::string::npos) wgt=35.6/985001;
 if(FileName.find("WJets") != std::string::npos) wgt=61526.7/2.82105e+07;
 //if(FileName.find("WJetsHT0To100") != std::string::npos) wgt=61526.7/47161328;
 //if(FileName.find("WJetsHT100To200") != std::string::npos) wgt=(1345*1.21)/10205377;
 //if(FileName.find("WJetsHT200To400") != std::string::npos) wgt=(359.7*1.21)/4949568;
 //if(FileName.find("WJetsHT400To600") != std::string::npos) wgt=(48.91*1.21)/1943664;
 //if(FileName.find("WJetsHT600To800") != std::string::npos) wgt=(12.05*1.21)/3767766;
 //if(FileName.find("WJetsHT800To1200") != std::string::npos) wgt=(5.501*1.21)/1568277;
 //if(FileName.find("WJetsHT1200To2500") != std::string::npos) wgt=(1.329*1.21)/246239;
 //if(FileName.find("WJetsHT2500ToInf") != std::string::npos) wgt=(0.03216*1.21)/251982;
 if(FileName.find("WW") != std::string::npos) wgt=118.70838/993215;
 if(FileName.find("WZ") != std::string::npos) wgt=47.13/1e+06;
 if(FileName.find("ZZ") != std::string::npos) wgt=16.523/989314;
 return wgt;
}
//Utils
double deltaPhi(double phi1, double phi2){
 double result = phi1 - phi2;
 while (result > M_PI) result -= 2*M_PI;
 while (result <= -M_PI) result += 2*M_PI;
 return result;
};
double deltaEta(double eta1, double eta2){
 return (eta1-eta2);
};
double deltaR(double dphi, double deta){
 return sqrt(pow(dphi,2)+pow(deta,2));
};
//Variable handleling
void rSetBranchAddress(TTree* readingtree, string sample){
 //Trigger
 //readingtree->SetBranchAddress("HLT_IsoMu22",&rHLT_IsoMu22,&b_rHLT_IsoMu22);
 //readingtree->SetBranchAddress("HLT_IsoTkMu22",&rHLT_IsoTkMu22,&b_rHLT_IsoTkMu22);
 //readingtree->SetBranchAddress("HLT_Ele27_eta2p1_WPTight_Gsf",&rHLT_Ele27_eta2p1_WPTight_Gsf,&b_rHLT_Ele27_eta2p1_WPTight_Gsf);
 //Muon
 readingtree->SetBranchAddress("Muon_pt",&rMuon_pt,&b_rMuon_pt);
 readingtree->SetBranchAddress("Muon_eta",&rMuon_eta,&b_rMuon_eta);
 readingtree->SetBranchAddress("Muon_phi",&rMuon_phi,&b_rMuon_phi);
 readingtree->SetBranchAddress("Muon_energy",&rMuon_energy,&b_rMuon_energy);
 readingtree->SetBranchAddress("Muon_tight",&rMuon_tight,&b_rMuon_tight);
 readingtree->SetBranchAddress("Muon_relIsoDeltaBetaR04",&rMuon_relIsoDeltaBetaR04,&b_rMuon_relIsoDeltaBetaR04);
 //Electron
 readingtree->SetBranchAddress("patElectron_pt",&rpatElectron_pt,&b_rpatElectron_pt);
 readingtree->SetBranchAddress("patElectron_eta",&rpatElectron_eta,&b_rpatElectron_eta);
 readingtree->SetBranchAddress("patElectron_phi",&rpatElectron_phi,&b_rpatElectron_phi);
 readingtree->SetBranchAddress("patElectron_energy",&rpatElectron_energy,&b_rpatElectron_energy);
 readingtree->SetBranchAddress("patElectron_inCrack",&rpatElectron_inCrack,&b_rpatElectron_inCrack);
 readingtree->SetBranchAddress("patElectron_isPassMvanontrig",&rpatElectron_isPassMvanontrig,&b_rpatElectron_isPassMvanontrig);
 readingtree->SetBranchAddress("patElectron_relIsoRhoEA",&rpatElectron_relIsoRhoEA,&b_rpatElectron_relIsoRhoEA);
 readingtree->SetBranchAddress("patElectron_SCeta",&rpatElectron_SCeta,&b_rpatElectron_SCeta);
 readingtree->SetBranchAddress("patElectron_full5x5_sigmaIetaIeta",&rpatElectron_full5x5_sigmaIetaIeta,&b_rpatElectron_full5x5_sigmaIetaIeta);
 readingtree->SetBranchAddress("patElectron_hOverE",&rpatElectron_hOverE,&b_rpatElectron_hOverE);
 readingtree->SetBranchAddress("patElectron_ecalPFClusterIso",&rpatElectron_ecalPFClusterIso,&b_rpatElectron_ecalPFClusterIso);
 readingtree->SetBranchAddress("patElectron_hcalPFClusterIso",&rpatElectron_hcalPFClusterIso,&b_rpatElectron_hcalPFClusterIso);
 readingtree->SetBranchAddress("patElectron_isolPtTracks",&rpatElectron_isolPtTracks,&b_rpatElectron_isolPtTracks);
 readingtree->SetBranchAddress("patElectron_dEtaIn",&rpatElectron_dEtaIn,&b_rpatElectron_dEtaIn);
 readingtree->SetBranchAddress("patElectron_dPhiIn",&rpatElectron_dPhiIn,&b_rpatElectron_dPhiIn);
 //Jet
 readingtree->SetBranchAddress("Jet_pt",&rJet_pt,&b_rJet_pt);
 readingtree->SetBranchAddress("Jet_eta",&rJet_eta,&b_rJet_eta);
 readingtree->SetBranchAddress("Jet_phi",&rJet_phi,&b_rJet_phi);
 readingtree->SetBranchAddress("Jet_energy",&rJet_energy,&b_rJet_energy);
 readingtree->SetBranchAddress("Jet_Uncorr_pt",&rJet_Uncorr_pt,&b_rJet_Uncorr_pt);
 readingtree->SetBranchAddress("Jet_JesSF",&rJet_JesSF,&b_rJet_JesSF);
 readingtree->SetBranchAddress("Jet_JerSF",&rJet_JerSF,&b_rJet_JerSF);
 readingtree->SetBranchAddress("Jet_neutralHadEnergyFraction",&rJet_neutralHadEnergyFraction,&b_rJet_neutralHadEnergyFraction);
 readingtree->SetBranchAddress("Jet_neutralEmEnergyFraction",&rJet_neutralEmEnergyFraction,&b_rJet_neutralEmEnergyFraction);
 readingtree->SetBranchAddress("Jet_chargedMultiplicity",&rJet_chargedMultiplicity,&b_rJet_chargedMultiplicity);
 readingtree->SetBranchAddress("Jet_numberOfConstituents",&rJet_numberOfConstituents,&b_rJet_numberOfConstituents);
 readingtree->SetBranchAddress("Jet_chargedHadronEnergyFraction",&rJet_chargedHadronEnergyFraction,&b_rJet_chargedHadronEnergyFraction);
 readingtree->SetBranchAddress("Jet_chargedEmEnergyFraction",&rJet_chargedEmEnergyFraction,&b_rJet_chargedEmEnergyFraction);
 readingtree->SetBranchAddress("Jet_newpfCombinedInclusiveSecondaryVertexV2BJetTags",&rJet_newpfCombinedInclusiveSecondaryVertexV2BJetTags,&b_rJet_newpfCombinedInclusiveSecondaryVertexV2BJetTags);
 //BJetness
 readingtree->SetBranchAddress("BJetness_isSingleLepton",&rBJetness_isSingleLepton,&b_rBJetness_isSingleLepton);
 readingtree->SetBranchAddress("BJetness_isDoubleLepton",&rBJetness_isDoubleLepton,&b_rBJetness_isDoubleLepton);
 readingtree->SetBranchAddress("BJetness_numjet",&rBJetness_numjet,&b_rBJetness_numjet);
 readingtree->SetBranchAddress("BJetness_jetschpvass",&rBJetness_jetschpvass,&b_rBJetness_jetschpvass);
 readingtree->SetBranchAddress("BJetness_jetschfrompv",&rBJetness_jetschfrompv,&b_rBJetness_jetschfrompv);
 readingtree->SetBranchAddress("BJetness_jetschip3dval",&rBJetness_jetschip3dval,&b_rBJetness_jetschip3dval);
 readingtree->SetBranchAddress("BJetness_jetschip3dsig",&rBJetness_jetschip3dsig,&b_rBJetness_jetschip3dsig);
 readingtree->SetBranchAddress("BJetness_jetschip2dval",&rBJetness_jetschip2dval,&b_rBJetness_jetschip2dval);
 readingtree->SetBranchAddress("BJetness_jetschip2dsig",&rBJetness_jetschip2dsig,&b_rBJetness_jetschip2dsig);
 readingtree->SetBranchAddress("BJetness_jetschisgoodtrk",&rBJetness_jetschisgoodtrk,&b_rBJetness_jetschisgoodtrk);
 readingtree->SetBranchAddress("BJetness_jetschtrkpur",&rBJetness_jetschtrkpur,&b_rBJetness_jetschtrkpur);
 readingtree->SetBranchAddress("BJetness_jetschpt",&rBJetness_jetschpt,&b_rBJetness_jetschpt);
 readingtree->SetBranchAddress("BJetness_jetschen",&rBJetness_jetschen,&b_rBJetness_jetschen);
 readingtree->SetBranchAddress("BJetness_num_pdgid_eles",&rBJetness_num_pdgid_eles,&b_rBJetness_num_pdgid_eles);
 readingtree->SetBranchAddress("BJetness_num_soft_eles",&rBJetness_num_soft_eles,&b_rBJetness_num_soft_eles);
 readingtree->SetBranchAddress("BJetness_num_vetonoipnoiso_eles",&rBJetness_num_vetonoipnoiso_eles,&b_rBJetness_num_vetonoipnoiso_eles);
 readingtree->SetBranchAddress("BJetness_num_loosenoipnoiso_eles",&rBJetness_num_loosenoipnoiso_eles,&b_rBJetness_num_loosenoipnoiso_eles);
 readingtree->SetBranchAddress("BJetness_num_veto_eles",&rBJetness_num_veto_eles,&b_rBJetness_num_veto_eles);
 readingtree->SetBranchAddress("BJetness_num_loose_eles",&rBJetness_num_loose_eles,&b_rBJetness_num_loose_eles);
 readingtree->SetBranchAddress("BJetness_num_medium_eles",&rBJetness_num_medium_eles,&b_rBJetness_num_medium_eles);
 readingtree->SetBranchAddress("BJetness_num_tight_eles",&rBJetness_num_tight_eles,&b_rBJetness_num_tight_eles);
 readingtree->SetBranchAddress("BJetness_num_mvatrig_eles",&rBJetness_num_mvatrig_eles,&b_rBJetness_num_mvatrig_eles);
 readingtree->SetBranchAddress("BJetness_num_mvanontrig_eles",&rBJetness_num_mvanontrig_eles,&b_rBJetness_num_mvanontrig_eles);
 readingtree->SetBranchAddress("BJetness_num_mvatrigwp90_eles",&rBJetness_num_mvatrigwp90_eles,&b_rBJetness_num_mvatrigwp90_eles);
 readingtree->SetBranchAddress("BJetness_num_mvanontrigwp90_eles",&rBJetness_num_mvanontrigwp90_eles,&b_rBJetness_num_mvanontrigwp90_eles);
 readingtree->SetBranchAddress("BJetness_num_heep_eles",&rBJetness_num_heep_eles,&b_rBJetness_num_heep_eles);
 readingtree->SetBranchAddress("BJetness_num_pdgid_mus",&rBJetness_num_pdgid_mus,&b_rBJetness_num_pdgid_mus);
 readingtree->SetBranchAddress("BJetness_num_loose_mus",&rBJetness_num_loose_mus,&b_rBJetness_num_loose_mus);
 readingtree->SetBranchAddress("BJetness_num_soft_mus",&rBJetness_num_soft_mus,&b_rBJetness_num_soft_mus);
 readingtree->SetBranchAddress("BJetness_num_medium_mus",&rBJetness_num_medium_mus,&b_rBJetness_num_medium_mus);
 readingtree->SetBranchAddress("BJetness_num_tight_mus",&rBJetness_num_tight_mus,&b_rBJetness_num_tight_mus);
 readingtree->SetBranchAddress("BJetness_num_highpt_mus",&rBJetness_num_highpt_mus,&b_rBJetness_num_highpt_mus);
 readingtree->SetBranchAddress("BJetness_num_POGisGood_mus",&rBJetness_num_POGisGood_mus,&b_rBJetness_num_POGisGood_mus);
 readingtree->SetBranchAddress("BJetness_numjettrks",&rBJetness_numjettrks,&b_rBJetness_numjettrks);
 readingtree->SetBranchAddress("BJetness_numjettrkspv",&rBJetness_numjettrkspv,&b_rBJetness_numjettrkspv);
 readingtree->SetBranchAddress("BJetness_numjettrksnopv",&rBJetness_numjettrksnopv,&b_rBJetness_numjettrksnopv);
 readingtree->SetBranchAddress("BJetness_npvTrkOVcollTrk",&rBJetness_npvTrkOVcollTrk,&b_rBJetness_npvTrkOVcollTrk);
 readingtree->SetBranchAddress("BJetness_pvTrkOVcollTrk",&rBJetness_pvTrkOVcollTrk,&b_rBJetness_pvTrkOVcollTrk);
 readingtree->SetBranchAddress("BJetness_npvTrkOVpvTrk",&rBJetness_npvTrkOVpvTrk,&b_rBJetness_npvTrkOVpvTrk);
 readingtree->SetBranchAddress("BJetness_npvPtOVcollPt",&rBJetness_npvPtOVcollPt,&b_rBJetness_npvPtOVcollPt);
 readingtree->SetBranchAddress("BJetness_pvPtOVcollPt",&rBJetness_pvPtOVcollPt,&b_rBJetness_pvPtOVcollPt);
 readingtree->SetBranchAddress("BJetness_npvPtOVpvPt",&rBJetness_npvPtOVpvPt,&b_rBJetness_npvPtOVpvPt);
 readingtree->SetBranchAddress("BJetness_avnum2v",&rBJetness_avnum2v,&b_rBJetness_avnum2v);
 readingtree->SetBranchAddress("BJetness_avnumno2v",&rBJetness_avnumno2v,&b_rBJetness_avnumno2v);
 readingtree->SetBranchAddress("BJetness_avdca3d2t",&rBJetness_avdca3d2t,&b_rBJetness_avdca3d2t);
 readingtree->SetBranchAddress("BJetness_avdca3dno2t",&rBJetness_avdca3dno2t,&b_rBJetness_avdca3dno2t);
 readingtree->SetBranchAddress("BJetness_avdca3d",&rBJetness_avdca3d,&b_rBJetness_avdca3d);
 readingtree->SetBranchAddress("BJetness_avdca2d2t",&rBJetness_avdca2d2t,&b_rBJetness_avdca2d2t);
 readingtree->SetBranchAddress("BJetness_avdca2dno2t",&rBJetness_avdca2dno2t,&b_rBJetness_avdca2dno2t);
 readingtree->SetBranchAddress("BJetness_avdca2d",&rBJetness_avdca2d,&b_rBJetness_avdca2d);
 readingtree->SetBranchAddress("BJetness_chi2",&rBJetness_chi2,&b_rBJetness_chi2);
 readingtree->SetBranchAddress("BJetness_avprel",&rBJetness_avprel,&b_rBJetness_avprel);
 readingtree->SetBranchAddress("BJetness_avppar",&rBJetness_avppar,&b_rBJetness_avppar);
 readingtree->SetBranchAddress("BJetness_avetarel",&rBJetness_avetarel,&b_rBJetness_avetarel);
 readingtree->SetBranchAddress("BJetness_avetapar",&rBJetness_avetapar,&b_rBJetness_avetapar);
 readingtree->SetBranchAddress("BJetness_avdr",&rBJetness_avdr,&b_rBJetness_avdr);
 readingtree->SetBranchAddress("BJetness_avpreljetpt",&rBJetness_avpreljetpt,&b_rBJetness_avpreljetpt);
 readingtree->SetBranchAddress("BJetness_avpreljeten",&rBJetness_avpreljeten,&b_rBJetness_avpreljeten);
 readingtree->SetBranchAddress("BJetness_avpparjetpt",&rBJetness_avpparjetpt,&b_rBJetness_avpparjetpt);
 readingtree->SetBranchAddress("BJetness_avpparjeten",&rBJetness_avpparjeten,&b_rBJetness_avpparjeten);
 readingtree->SetBranchAddress("BJetness_avip3d_val",&rBJetness_avip3d_val,&b_rBJetness_avip3d_val);
 readingtree->SetBranchAddress("BJetness_avip3d_sig",&rBJetness_avip3d_sig,&b_rBJetness_avip3d_sig);
 readingtree->SetBranchAddress("BJetness_avsip3d_val",&rBJetness_avsip3d_val,&b_rBJetness_avsip3d_val);
 readingtree->SetBranchAddress("BJetness_avsip3d_sig",&rBJetness_avsip3d_sig,&b_rBJetness_avsip3d_sig);
 readingtree->SetBranchAddress("BJetness_numip3dpos",&rBJetness_numip3dpos,&b_rBJetness_numip3dpos);
 readingtree->SetBranchAddress("BJetness_numip3dneg",&rBJetness_numip3dneg,&b_rBJetness_numip3dneg);
 readingtree->SetBranchAddress("BJetness_avip2d_val",&rBJetness_avip2d_val,&b_rBJetness_avip2d_val);
 readingtree->SetBranchAddress("BJetness_avip2d_sig",&rBJetness_avip2d_sig,&b_rBJetness_avip2d_sig);
 readingtree->SetBranchAddress("BJetness_avsip2d_val",&rBJetness_avsip2d_val,&b_rBJetness_avsip2d_val);
 readingtree->SetBranchAddress("BJetness_avsip2d_sig",&rBJetness_avsip2d_sig,&b_rBJetness_avsip2d_sig);
 readingtree->SetBranchAddress("BJetness_numip2dpos",&rBJetness_numip2dpos,&b_rBJetness_numip2dpos);
 readingtree->SetBranchAddress("BJetness_numip2dneg",&rBJetness_numip2dneg,&b_rBJetness_numip2dneg);
 readingtree->SetBranchAddress("BJetness_avip1d_val",&rBJetness_avip1d_val,&b_rBJetness_avip1d_val);
 readingtree->SetBranchAddress("BJetness_avip1d_sig",&rBJetness_avip1d_sig,&b_rBJetness_avip1d_sig);
 readingtree->SetBranchAddress("BJetness_avsip1d_val",&rBJetness_avsip1d_val,&b_rBJetness_avsip1d_val);
 readingtree->SetBranchAddress("BJetness_avsip1d_sig",&rBJetness_avsip1d_sig,&b_rBJetness_avsip1d_sig);
 readingtree->SetBranchAddress("BJetnessFV_num_leps",&rBJetnessFV_num_leps,&b_rBJetnessFV_num_leps);
 readingtree->SetBranchAddress("BJetnessFV_npvTrkOVcollTrk",&rBJetnessFV_npvTrkOVcollTrk,&b_rBJetnessFV_npvTrkOVcollTrk);
 readingtree->SetBranchAddress("BJetnessFV_avip3d_val",&rBJetnessFV_avip3d_val,&b_rBJetnessFV_avip3d_val);
 readingtree->SetBranchAddress("BJetnessFV_avip3d_sig",&rBJetnessFV_avip3d_sig,&b_rBJetnessFV_avip3d_sig);
 readingtree->SetBranchAddress("BJetnessFV_avsip3d_sig",&rBJetnessFV_avsip3d_sig,&b_rBJetnessFV_avsip3d_sig);
 readingtree->SetBranchAddress("BJetnessFV_avip1d_sig",&rBJetnessFV_avip1d_sig,&b_rBJetnessFV_avip1d_sig);
 //Weights
 readingtree->SetBranchAddress("bWeight",&rbWeight,&b_rbWeight);
 readingtree->SetBranchAddress("nBestVtx",&rnBestVtx,&b_rnBestVtx);
 readingtree->SetBranchAddress("PUWeight",&rPUWeight,&b_rPUWeight);
 readingtree->SetBranchAddress("EVENT_genWeight",&rEVENT_genWeight,&b_rEVENT_genWeight);
 if(sample!="data") readingtree->SetBranchAddress("trueInteractions",&rtrueInteractions,&b_rtrueInteractions);
 //Evt
 if(sample!="data") readingtree->SetBranchAddress("ttHFCategory",&rttHFCategory,&b_rttHFCategory); 
}
void wSetBranchAddress(TTree* newtree, string sample){
 //Muon
 newtree->Branch("Muon_pt",&Muon_pt);
 newtree->Branch("Muon_eta",&Muon_eta);
 newtree->Branch("Muon_phi",&Muon_phi);
 newtree->Branch("Muon_energy",&Muon_energy);
 newtree->Branch("Muon_tight",&Muon_tight);
 newtree->Branch("Muon_relIsoDeltaBetaR04",&Muon_relIsoDeltaBetaR04);
 newtree->Branch("Muon_IDSFval",&Muon_IDSFval);
 newtree->Branch("Muon_IsoSFval",&Muon_IsoSFval);
 newtree->Branch("Muon_TrkSFval",&Muon_TrkSFval);
 newtree->Branch("Muon_IDSFerr",&Muon_IDSFerr);
 newtree->Branch("Muon_IsoSFerr",&Muon_IsoSFerr);
 newtree->Branch("Muon_TrkSFerr",&Muon_TrkSFerr);
 //Electron
 newtree->Branch("patElectron_pt",&patElectron_pt);
 newtree->Branch("patElectron_eta",&patElectron_eta);
 newtree->Branch("patElectron_phi",&patElectron_phi);
 newtree->Branch("patElectron_energy",&patElectron_energy);
 newtree->Branch("patElectron_inCrack",&patElectron_inCrack);
 newtree->Branch("patElectron_isPassMvanontrig",&patElectron_isPassMvanontrig);
 newtree->Branch("patElectron_relIsoRhoEA",&patElectron_relIsoRhoEA);
 newtree->Branch("patElectron_SCeta",&patElectron_SCeta);
 newtree->Branch("patElectron_full5x5_sigmaIetaIeta",&patElectron_full5x5_sigmaIetaIeta);
 newtree->Branch("patElectron_hOverE",&patElectron_hOverE);
 newtree->Branch("patElectron_ecalPFClusterIso",&patElectron_ecalPFClusterIso);
 newtree->Branch("patElectron_hcalPFClusterIso",&patElectron_hcalPFClusterIso);
 newtree->Branch("patElectron_isolPtTracks",&patElectron_isolPtTracks);
 newtree->Branch("patElectron_dEtaIn",&patElectron_dEtaIn);
 newtree->Branch("patElectron_dPhiIn",&patElectron_dPhiIn);
 newtree->Branch("patElectron_IDSFval",&patElectron_IDSFval);
 newtree->Branch("patElectron_GsfSFval",&patElectron_GsfSFval);
 newtree->Branch("patElectron_IDSFerr",&patElectron_IDSFerr);
 newtree->Branch("patElectron_GsfSFerr",&patElectron_GsfSFerr);
 //Jet
 newtree->Branch("Jet_pt",&Jet_pt);
 newtree->Branch("Jet_eta",&Jet_eta);
 newtree->Branch("Jet_phi",&Jet_phi);
 newtree->Branch("Jet_energy",&Jet_energy);
 newtree->Branch("Jet_Uncorr_pt",&Jet_Uncorr_pt);
 newtree->Branch("Jet_JesSF",&Jet_JesSF);
 newtree->Branch("Jet_JerSF",&Jet_JerSF);
 newtree->Branch("Jet_neutralHadEnergyFraction",&Jet_neutralHadEnergyFraction);
 newtree->Branch("Jet_neutralEmEnergyFraction",&Jet_neutralEmEnergyFraction);
 newtree->Branch("Jet_chargedMultiplicity",&Jet_chargedMultiplicity);
 newtree->Branch("Jet_numberOfConstituents",&Jet_numberOfConstituents);
 newtree->Branch("Jet_chargedHadronEnergyFraction",&Jet_chargedHadronEnergyFraction);
 newtree->Branch("Jet_chargedEmEnergyFraction",&Jet_chargedEmEnergyFraction);
 newtree->Branch("Jet_newpfCombinedInclusiveSecondaryVertexV2BJetTags",&Jet_newpfCombinedInclusiveSecondaryVertexV2BJetTags);
 //BJetness
 newtree->Branch("BJetness_isSingleLepton",&BJetness_isSingleLepton);
 newtree->Branch("BJetness_isDoubleLepton",&BJetness_isDoubleLepton);
 newtree->Branch("BJetness_numjet",&BJetness_numjet);
 newtree->Branch("BJetness_jetschpvass",&BJetness_jetschpvass);
 newtree->Branch("BJetness_jetschfrompv",&BJetness_jetschfrompv);
 newtree->Branch("BJetness_jetschip3dval",&BJetness_jetschip3dval);
 newtree->Branch("BJetness_jetschip3dsig",&BJetness_jetschip3dsig);
 newtree->Branch("BJetness_jetschip2dval",&BJetness_jetschip2dval);
 newtree->Branch("BJetness_jetschip2dsig",&BJetness_jetschip2dsig);
 newtree->Branch("BJetness_jetschisgoodtrk",&BJetness_jetschisgoodtrk);
 newtree->Branch("BJetness_jetschtrkpur",&BJetness_jetschtrkpur);
 newtree->Branch("BJetness_jetschpt",&BJetness_jetschpt);
 newtree->Branch("BJetness_jetschen",&BJetness_jetschen);
 newtree->Branch("BJetness_num_pdgid_eles",&BJetness_num_pdgid_eles);
 newtree->Branch("BJetness_num_soft_eles",&BJetness_num_soft_eles);
 newtree->Branch("BJetness_num_vetonoipnoiso_eles",&BJetness_num_vetonoipnoiso_eles);
 newtree->Branch("BJetness_num_loosenoipnoiso_eles",&BJetness_num_loosenoipnoiso_eles);
 newtree->Branch("BJetness_num_veto_eles",&BJetness_num_veto_eles);
 newtree->Branch("BJetness_num_loose_eles",&BJetness_num_loose_eles);
 newtree->Branch("BJetness_num_medium_eles",&BJetness_num_medium_eles);
 newtree->Branch("BJetness_num_tight_eles",&BJetness_num_tight_eles);
 newtree->Branch("BJetness_num_mvatrig_eles",&BJetness_num_mvatrig_eles);
 newtree->Branch("BJetness_num_mvanontrig_eles",&BJetness_num_mvanontrig_eles);
 newtree->Branch("BJetness_num_mvatrigwp90_eles",&BJetness_num_mvatrigwp90_eles);
 newtree->Branch("BJetness_num_mvanontrigwp90_eles",&BJetness_num_mvanontrigwp90_eles);
 newtree->Branch("BJetness_num_heep_eles",&BJetness_num_heep_eles);
 newtree->Branch("BJetness_num_pdgid_mus",&BJetness_num_pdgid_mus);
 newtree->Branch("BJetness_num_loose_mus",&BJetness_num_loose_mus);
 newtree->Branch("BJetness_num_soft_mus",&BJetness_num_soft_mus);
 newtree->Branch("BJetness_num_medium_mus",&BJetness_num_medium_mus);
 newtree->Branch("BJetness_num_tight_mus",&BJetness_num_tight_mus);
 newtree->Branch("BJetness_num_highpt_mus",&BJetness_num_highpt_mus);
 newtree->Branch("BJetness_num_POGisGood_mus",&BJetness_num_POGisGood_mus);
 newtree->Branch("BJetness_numjettrks",&BJetness_numjettrks);
 newtree->Branch("BJetness_numjettrkspv",&BJetness_numjettrkspv);
 newtree->Branch("BJetness_numjettrksnopv",&BJetness_numjettrksnopv);
 newtree->Branch("BJetness_npvTrkOVcollTrk",&BJetness_npvTrkOVcollTrk);
 newtree->Branch("BJetness_pvTrkOVcollTrk",&BJetness_pvTrkOVcollTrk);
 newtree->Branch("BJetness_npvTrkOVpvTrk",&BJetness_npvTrkOVpvTrk);
 newtree->Branch("BJetness_npvPtOVcollPt",&BJetness_npvPtOVcollPt);
 newtree->Branch("BJetness_pvPtOVcollPt",&BJetness_pvPtOVcollPt);
 newtree->Branch("BJetness_npvPtOVpvPt",&BJetness_npvPtOVpvPt);
 newtree->Branch("BJetness_avnum2v",&BJetness_avnum2v);
 newtree->Branch("BJetness_avnumno2v",&BJetness_avnumno2v);
 newtree->Branch("BJetness_avdca3d2t",&BJetness_avdca3d2t);
 newtree->Branch("BJetness_avdca3dno2t",&BJetness_avdca3dno2t);
 newtree->Branch("BJetness_avdca3d",&BJetness_avdca3d);
 newtree->Branch("BJetness_avdca2d2t",&BJetness_avdca2d2t);
 newtree->Branch("BJetness_avdca2dno2t",&BJetness_avdca2dno2t);
 newtree->Branch("BJetness_avdca2d",&BJetness_avdca2d);
 newtree->Branch("BJetness_chi2",&BJetness_chi2);
 newtree->Branch("BJetness_avprel",&BJetness_avprel);
 newtree->Branch("BJetness_avppar",&BJetness_avppar);
 newtree->Branch("BJetness_avetarel",&BJetness_avetarel);
 newtree->Branch("BJetness_avetapar",&BJetness_avetapar);
 newtree->Branch("BJetness_avdr",&BJetness_avdr);
 newtree->Branch("BJetness_avpreljetpt",&BJetness_avpreljetpt);
 newtree->Branch("BJetness_avpreljeten",&BJetness_avpreljeten);
 newtree->Branch("BJetness_avpparjetpt",&BJetness_avpparjetpt);
 newtree->Branch("BJetness_avpparjeten",&BJetness_avpparjeten);
 newtree->Branch("BJetness_avip3d_val",&BJetness_avip3d_val);
 newtree->Branch("BJetness_avip3d_sig",&BJetness_avip3d_sig);
 newtree->Branch("BJetness_avsip3d_val",&BJetness_avsip3d_val);
 newtree->Branch("BJetness_avsip3d_sig",&BJetness_avsip3d_sig);
 newtree->Branch("BJetness_numip3dpos",&BJetness_numip3dpos);
 newtree->Branch("BJetness_numip3dneg",&BJetness_numip3dneg);
 newtree->Branch("BJetness_avip2d_val",&BJetness_avip2d_val);
 newtree->Branch("BJetness_avip2d_sig",&BJetness_avip2d_sig);
 newtree->Branch("BJetness_avsip2d_val",&BJetness_avsip2d_val);
 newtree->Branch("BJetness_avsip2d_sig",&BJetness_avsip2d_sig);
 newtree->Branch("BJetness_numip2dpos",&BJetness_numip2dpos);
 newtree->Branch("BJetness_numip2dneg",&BJetness_numip2dneg);
 newtree->Branch("BJetness_avip1d_val",&BJetness_avip1d_val);
 newtree->Branch("BJetness_avip1d_sig",&BJetness_avip1d_sig);
 newtree->Branch("BJetness_avsip1d_val",&BJetness_avsip1d_val);
 newtree->Branch("BJetness_avsip1d_sig",&BJetness_avsip1d_sig);
 newtree->Branch("BJetnessFV_num_leps",&BJetnessFV_num_leps);
 newtree->Branch("BJetnessFV_npvTrkOVcollTrk",&BJetnessFV_npvTrkOVcollTrk);
 newtree->Branch("BJetnessFV_avip3d_val",&BJetnessFV_avip3d_val);
 newtree->Branch("BJetnessFV_avip3d_sig",&BJetnessFV_avip3d_sig);
 newtree->Branch("BJetnessFV_avsip3d_sig",&BJetnessFV_avsip3d_sig);
 newtree->Branch("BJetnessFV_avip1d_sig",&BJetnessFV_avip1d_sig);
 //Weights
 newtree->Branch("bWeight",&bWeight);
 newtree->Branch("nBestVtx",&nBestVtx);
 newtree->Branch("PUWeight",&PUWeight);
 newtree->Branch("EVENT_genWeight",&EVENT_genWeight);
 newtree->Branch("lumi_wgt",&lumi_wgt);
 newtree->Branch("lepsf",&lepsf);
 //Evt
 newtree->Branch("ttHFCategory",&ttHFCategory);
 //New variables
 //Evt
 newtree->Branch("isSingleMuonEvt",&isSingleMuonEvt);
 newtree->Branch("isSingleElectronEvt",&isSingleElectronEvt);
 newtree->Branch("Jet_num",&Jet_num);
 newtree->Branch("Jet_numbLoose",&Jet_numbLoose);
 newtree->Branch("Jet_numbMedium",&Jet_numbMedium);
 newtree->Branch("Jet_numbTight",&Jet_numbTight);
}
void wClearInitialization(string sample){
 //Muon
 Muon_pt->clear();
 Muon_eta->clear();
 Muon_phi->clear();
 Muon_energy->clear();
 Muon_tight->clear();
 Muon_relIsoDeltaBetaR04->clear();
 Muon_IDSFval->clear();
 Muon_IsoSFval->clear();
 Muon_TrkSFval->clear();
 Muon_IDSFerr->clear();
 Muon_IsoSFerr->clear();
 Muon_TrkSFerr->clear();
 //Electron
 patElectron_pt->clear();
 patElectron_eta->clear();
 patElectron_phi->clear();
 patElectron_energy->clear();
 patElectron_inCrack->clear();
 patElectron_isPassMvanontrig->clear();
 patElectron_relIsoRhoEA->clear();
 patElectron_SCeta->clear();
 patElectron_full5x5_sigmaIetaIeta->clear();
 patElectron_hOverE->clear();
 patElectron_ecalPFClusterIso->clear();
 patElectron_hcalPFClusterIso->clear();
 patElectron_isolPtTracks->clear();
 patElectron_dEtaIn->clear();
 patElectron_dPhiIn->clear();
 patElectron_IDSFval->clear();
 patElectron_GsfSFval->clear();
 patElectron_IDSFerr->clear();
 patElectron_GsfSFerr->clear();
 //Jet
 Jet_pt->clear();
 Jet_eta->clear();
 Jet_phi->clear();
 Jet_energy->clear();
 Jet_Uncorr_pt->clear();
 Jet_JesSF->clear();
 Jet_JerSF->clear();
 Jet_neutralHadEnergyFraction->clear();
 Jet_neutralEmEnergyFraction->clear();
 Jet_chargedMultiplicity->clear();
 Jet_numberOfConstituents->clear();
 Jet_chargedHadronEnergyFraction->clear();
 Jet_chargedEmEnergyFraction->clear();
 Jet_newpfCombinedInclusiveSecondaryVertexV2BJetTags->clear();
 //BJetness
 BJetness_isSingleLepton = -999;
 BJetness_isDoubleLepton = -999;
 BJetness_numjet = -999;
 BJetness_jetschpvass->clear();
 BJetness_jetschfrompv->clear();
 BJetness_jetschip3dval->clear();
 BJetness_jetschip3dsig->clear();
 BJetness_jetschip2dval->clear();
 BJetness_jetschip2dsig->clear();
 BJetness_jetschisgoodtrk->clear();
 BJetness_jetschtrkpur->clear();
 BJetness_jetschpt->clear();
 BJetness_jetschen->clear();
 BJetness_num_pdgid_eles = -999;
 BJetness_num_soft_eles = -999;
 BJetness_num_vetonoipnoiso_eles = -999;
 BJetness_num_loosenoipnoiso_eles = -999;
 BJetness_num_veto_eles = -999;
 BJetness_num_loose_eles = -999;
 BJetness_num_medium_eles = -999;
 BJetness_num_tight_eles = -999;
 BJetness_num_mvatrig_eles = -999;
 BJetness_num_mvanontrig_eles = -999;
 BJetness_num_mvatrigwp90_eles = -999;
 BJetness_num_mvanontrigwp90_eles = -999;
 BJetness_num_heep_eles = -999;
 BJetness_num_pdgid_mus = -999;
 BJetness_num_loose_mus = -999;
 BJetness_num_soft_mus = -999;
 BJetness_num_medium_mus = -999;
 BJetness_num_tight_mus = -999;
 BJetness_num_highpt_mus = -999;
 BJetness_num_POGisGood_mus = -999;
 BJetness_numjettrks = -999;
 BJetness_numjettrkspv = -999;
 BJetness_numjettrksnopv = -999;
 BJetness_npvTrkOVcollTrk = -999;
 BJetness_pvTrkOVcollTrk = -999;
 BJetness_npvTrkOVpvTrk = -999;
 BJetness_npvPtOVcollPt = -999;
 BJetness_pvPtOVcollPt = -999;
 BJetness_npvPtOVpvPt = -999;
 BJetness_avnum2v = -999;
 BJetness_avnumno2v = -999;
 BJetness_avdca3d2t = -999;
 BJetness_avdca3dno2t = -999;
 BJetness_avdca3d = -999;
 BJetness_avdca2d2t = -999;
 BJetness_avdca2dno2t = -999;
 BJetness_avdca2d = -999;
 BJetness_chi2 = -999;
 BJetness_avprel = -999;
 BJetness_avppar = -999;
 BJetness_avetarel = -999;
 BJetness_avetapar = -999;
 BJetness_avdr = -999;
 BJetness_avpreljetpt = -999;
 BJetness_avpreljeten = -999;
 BJetness_avpparjetpt = -999;
 BJetness_avpparjeten = -999;
 BJetness_avip3d_val = -999;
 BJetness_avip3d_sig = -999;
 BJetness_avsip3d_val = -999;
 BJetness_avsip3d_sig = -999;
 BJetness_numip3dpos = -999;
 BJetness_numip3dneg = -999;
 BJetness_avip2d_val = -999;
 BJetness_avip2d_sig = -999;
 BJetness_avsip2d_val = -999;
 BJetness_avsip2d_sig = -999;
 BJetness_numip2dpos = -999;
 BJetness_numip2dneg = -999;
 BJetness_avip1d_val = -999;
 BJetness_avip1d_sig = -999;
 BJetness_avsip1d_val = -999;
 BJetness_avsip1d_sig = -999;
 BJetnessFV_num_leps = -999;
 BJetnessFV_npvTrkOVcollTrk = -999;
 BJetnessFV_avip3d_val = -999;
 BJetnessFV_avip3d_sig = -999;
 BJetnessFV_avsip3d_sig = -999;
 BJetnessFV_avip1d_sig = -999;
 //Weights
 bWeight      = -999;
 nBestVtx     = -999;
 PUWeight     = -999;
 EVENT_genWeight = -999;
 lumi_wgt        = -999;
 lepsf           = 1;
 //Evt
 ttHFCategory = -999; 
 //New variables
 //Evt
 isSingleMuonEvt = 0;
 isSingleElectronEvt = 0;
 Jet_num        = 0;
 Jet_numbLoose  = 0;
 Jet_numbMedium = 0;
 Jet_numbTight  = 0;
}
void rGetEntry(Long64_t tentry, string sample){
 //Trigger
 //b_rHLT_IsoMu22->GetEntry(tentry);
 //b_rHLT_IsoTkMu22->GetEntry(tentry);
 //b_rHLT_Ele27_eta2p1_WPTight_Gsf->GetEntry(tentry);
 //Muon
 b_rMuon_pt->GetEntry(tentry);
 b_rMuon_eta->GetEntry(tentry);
 b_rMuon_phi->GetEntry(tentry);
 b_rMuon_energy->GetEntry(tentry);
 b_rMuon_tight->GetEntry(tentry);
 b_rMuon_relIsoDeltaBetaR04->GetEntry(tentry);
 //Electron
 b_rpatElectron_pt->GetEntry(tentry);
 b_rpatElectron_eta->GetEntry(tentry);
 b_rpatElectron_phi->GetEntry(tentry);
 b_rpatElectron_energy->GetEntry(tentry);
 b_rpatElectron_inCrack->GetEntry(tentry);
 b_rpatElectron_isPassMvanontrig->GetEntry(tentry);
 b_rpatElectron_relIsoRhoEA->GetEntry(tentry);
 b_rpatElectron_SCeta->GetEntry(tentry);
 b_rpatElectron_full5x5_sigmaIetaIeta->GetEntry(tentry);
 b_rpatElectron_hOverE->GetEntry(tentry);
 b_rpatElectron_ecalPFClusterIso->GetEntry(tentry);
 b_rpatElectron_hcalPFClusterIso->GetEntry(tentry);
 b_rpatElectron_isolPtTracks->GetEntry(tentry);
 b_rpatElectron_dEtaIn->GetEntry(tentry);
 b_rpatElectron_dPhiIn->GetEntry(tentry);
 //Jet
 b_rJet_pt->GetEntry(tentry);
 b_rJet_eta->GetEntry(tentry);
 b_rJet_phi->GetEntry(tentry);
 b_rJet_energy->GetEntry(tentry);
 b_rJet_Uncorr_pt->GetEntry(tentry);
 b_rJet_JesSF->GetEntry(tentry);
 b_rJet_JerSF->GetEntry(tentry);
 b_rJet_neutralHadEnergyFraction->GetEntry(tentry);
 b_rJet_neutralEmEnergyFraction->GetEntry(tentry);
 b_rJet_chargedMultiplicity->GetEntry(tentry);
 b_rJet_numberOfConstituents->GetEntry(tentry);
 b_rJet_chargedHadronEnergyFraction->GetEntry(tentry);
 b_rJet_chargedEmEnergyFraction->GetEntry(tentry);
 b_rJet_newpfCombinedInclusiveSecondaryVertexV2BJetTags->GetEntry(tentry);
 //BJetness
 b_rBJetness_isSingleLepton->GetEntry(tentry);
 b_rBJetness_isDoubleLepton->GetEntry(tentry);
 b_rBJetness_numjet->GetEntry(tentry);
 b_rBJetness_jetschpvass->GetEntry(tentry);
 b_rBJetness_jetschfrompv->GetEntry(tentry);
 b_rBJetness_jetschip3dval->GetEntry(tentry);
 b_rBJetness_jetschip3dsig->GetEntry(tentry);
 b_rBJetness_jetschip2dval->GetEntry(tentry);
 b_rBJetness_jetschip2dsig->GetEntry(tentry);
 b_rBJetness_jetschisgoodtrk->GetEntry(tentry);
 b_rBJetness_jetschtrkpur->GetEntry(tentry);
 b_rBJetness_jetschpt->GetEntry(tentry);
 b_rBJetness_jetschen->GetEntry(tentry);
 b_rBJetness_num_pdgid_eles->GetEntry(tentry);
 b_rBJetness_num_soft_eles->GetEntry(tentry);
 b_rBJetness_num_vetonoipnoiso_eles->GetEntry(tentry);
 b_rBJetness_num_loosenoipnoiso_eles->GetEntry(tentry);
 b_rBJetness_num_veto_eles->GetEntry(tentry);
 b_rBJetness_num_loose_eles->GetEntry(tentry);
 b_rBJetness_num_medium_eles->GetEntry(tentry);
 b_rBJetness_num_tight_eles->GetEntry(tentry);
 b_rBJetness_num_mvatrig_eles->GetEntry(tentry);
 b_rBJetness_num_mvanontrig_eles->GetEntry(tentry);
 b_rBJetness_num_mvatrigwp90_eles->GetEntry(tentry);
 b_rBJetness_num_mvanontrigwp90_eles->GetEntry(tentry);
 b_rBJetness_num_heep_eles->GetEntry(tentry);
 b_rBJetness_num_pdgid_mus->GetEntry(tentry);
 b_rBJetness_num_loose_mus->GetEntry(tentry);
 b_rBJetness_num_soft_mus->GetEntry(tentry);
 b_rBJetness_num_medium_mus->GetEntry(tentry);
 b_rBJetness_num_tight_mus->GetEntry(tentry);
 b_rBJetness_num_highpt_mus->GetEntry(tentry);
 b_rBJetness_num_POGisGood_mus->GetEntry(tentry);
 b_rBJetness_numjettrks->GetEntry(tentry);
 b_rBJetness_numjettrkspv->GetEntry(tentry);
 b_rBJetness_numjettrksnopv->GetEntry(tentry);
 b_rBJetness_npvTrkOVcollTrk->GetEntry(tentry);
 b_rBJetness_pvTrkOVcollTrk->GetEntry(tentry);
 b_rBJetness_npvTrkOVpvTrk->GetEntry(tentry);
 b_rBJetness_npvPtOVcollPt->GetEntry(tentry);
 b_rBJetness_pvPtOVcollPt->GetEntry(tentry);
 b_rBJetness_npvPtOVpvPt->GetEntry(tentry);
 b_rBJetness_avnum2v->GetEntry(tentry);
 b_rBJetness_avnumno2v->GetEntry(tentry);
 b_rBJetness_avdca3d2t->GetEntry(tentry);
 b_rBJetness_avdca3dno2t->GetEntry(tentry);
 b_rBJetness_avdca3d->GetEntry(tentry);
 b_rBJetness_avdca2d2t->GetEntry(tentry);
 b_rBJetness_avdca2dno2t->GetEntry(tentry);
 b_rBJetness_avdca2d->GetEntry(tentry);
 b_rBJetness_chi2->GetEntry(tentry);
 b_rBJetness_avprel->GetEntry(tentry);
 b_rBJetness_avppar->GetEntry(tentry);
 b_rBJetness_avetarel->GetEntry(tentry);
 b_rBJetness_avetapar->GetEntry(tentry);
 b_rBJetness_avdr->GetEntry(tentry);
 b_rBJetness_avpreljetpt->GetEntry(tentry);
 b_rBJetness_avpreljeten->GetEntry(tentry);
 b_rBJetness_avpparjetpt->GetEntry(tentry);
 b_rBJetness_avpparjeten->GetEntry(tentry);
 b_rBJetness_avip3d_val->GetEntry(tentry);
 b_rBJetness_avip3d_sig->GetEntry(tentry);
 b_rBJetness_avsip3d_val->GetEntry(tentry);
 b_rBJetness_avsip3d_sig->GetEntry(tentry);
 b_rBJetness_numip3dpos->GetEntry(tentry);
 b_rBJetness_numip3dneg->GetEntry(tentry);
 b_rBJetness_avip2d_val->GetEntry(tentry);
 b_rBJetness_avip2d_sig->GetEntry(tentry);
 b_rBJetness_avsip2d_val->GetEntry(tentry);
 b_rBJetness_avsip2d_sig->GetEntry(tentry);
 b_rBJetness_numip2dpos->GetEntry(tentry);
 b_rBJetness_numip2dneg->GetEntry(tentry);
 b_rBJetness_avip1d_val->GetEntry(tentry);
 b_rBJetness_avip1d_sig->GetEntry(tentry);
 b_rBJetness_avsip1d_val->GetEntry(tentry);
 b_rBJetness_avsip1d_sig->GetEntry(tentry);
 b_rBJetnessFV_num_leps->GetEntry(tentry);
 b_rBJetnessFV_npvTrkOVcollTrk->GetEntry(tentry);
 b_rBJetnessFV_avip3d_val->GetEntry(tentry);
 b_rBJetnessFV_avip3d_sig->GetEntry(tentry);
 b_rBJetnessFV_avsip3d_sig->GetEntry(tentry);
 b_rBJetnessFV_avip1d_sig->GetEntry(tentry);
 //Weights
 b_rbWeight->GetEntry(tentry);
 b_rnBestVtx->GetEntry(tentry);
 b_rPUWeight->GetEntry(tentry);
 b_rEVENT_genWeight->GetEntry(tentry);
 if(sample!="data") b_rtrueInteractions->GetEntry(tentry);
 //Evt
 if(sample!="data") b_rttHFCategory->GetEntry(tentry);
}
