#!/bin/bash
analysis=TTHbb_SL #HeavyNeutrino_DR #HeavyNeutrino_SigTopDY #EventCounter
series=TTHbb_
tasks=(
TTHbb
TTHnbb
TT
ST
SaT
TTWqq
DY
WJets
SEleB
SEleC
SEleD
SEleE
SEleF
SEleG
SMuB
SMuC
SMuD
SMuE
SMuF
SMuG
)
rm -r $analysis
mkdir $analysis
pos=0
for d in ${tasks[@]}; 
do
  cd $series${tasks[$pos]}/$analysis  
  rm ${tasks[$pos]}\_$analysis\.root
  hadd ${tasks[$pos]}\_$analysis\.root *root
  cd ../..
  mv $series${tasks[$pos]}/$analysis/${tasks[$pos]}\_$analysis\.root $analysis
  #rm -r $series${tasks[$pos]}/$analysis
  let pos=pos+1
done
