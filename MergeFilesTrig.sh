#!/bin/bash
analysis=TTHbb_SL #HeavyNeutrino_DR #HeavyNeutrino_SigTopDY #EventCounter
series=TTHbb
tasks=(
_TTHbb
2_TT
_SEleB
_SEleC
_SEleD
_SMuB
2_SMuC
_SMuD
)
datasets=(
ttHTobb_M125_13TeV_powheg_pythia8
TT_TuneCUETP8M1_13TeV-powheg-pythia8
SingleElectron
SingleElectron
SingleElectron
SingleMuon
SingleMuon
SingleMuon
)
rm -r $analysis
mkdir $analysis
pos=0
for d in ${tasks[@]}; 
do
  cd $series${tasks[$pos]}/$analysis  
  rm ${tasks[$pos]}\_$analysis\.root
  hadd ${tasks[$pos]}\_$analysis\.root *root
  cd ../..
  mv $series${tasks[$pos]}/$analysis/${tasks[$pos]}\_$analysis\.root $analysis
  #rm -r $series${tasks[$pos]}/$analysis
  let pos=pos+1
done
