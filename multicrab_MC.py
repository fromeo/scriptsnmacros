if __name__ == '__main__':
 #####
 ##   Multicrab configuration
 #####
 from CRABClient.UserUtilities import config, getUsernameFromSiteDB
 config = config()
 from CRABAPI.RawCommand import crabCommand
 from CRABClient.ClientExceptions import ClientException
 from httplib import HTTPException
 config.General.workArea = 'Crab_projects'
 
 def submit(config):
  try:
   crabCommand('submit', config = config)
  except HTTPException as hte:
   print "Failed submitting task: %s" % (hte.headers)
  except ClientException as cle:
   print "Failed submitting task: %s" % (cle)
 #####
 ##   Crab configuration
 #####
 datasetnames  = [
'TTHbb_TTHbb',
'TTHbb_TTHnbb',
'TTHbb_TT',
'TTHbb_ST',
'TTHbb_SaT',
'TTHbb_TTWqq',
#'TTHbb_TTZqq',
'TTHbb_DY',
'TTHbb_WJets'#,
#'TTHbb_WW',
#'TTHbb_WZ',
#'TTHbb_ZZ'
                 ]
 datasetinputs = [
'/ttHTobb_M125_TuneCUETP8M2_ttHtranche3_13TeV-powheg-pythia8/RunIISummer16MiniAODv2-PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/MINIAODSIM',
'/ttHToNonbb_M125_TuneCUETP8M2_ttHtranche3_13TeV-powheg-pythia8/RunIISummer16MiniAODv2-PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/MINIAODSIM',
'/TT_TuneCUETP8M2T4_13TeV-powheg-pythia8/RunIISummer16MiniAODv2-PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/MINIAODSIM',
'/ST_tW_top_5f_inclusiveDecays_13TeV-powheg-pythia8_TuneCUETP8M1/RunIISummer16MiniAODv2-PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6_ext1-v1/MINIAODSIM',
'/ST_tW_antitop_5f_inclusiveDecays_13TeV-powheg-pythia8_TuneCUETP8M2T4/RunIISummer16MiniAODv2-PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/MINIAODSIM',
'/TTWJetsToQQ_TuneCUETP8M1_13TeV-amcatnloFXFX-madspin-pythia8/RunIISummer16MiniAODv2-PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/MINIAODSIM',
#'/TTZToQQ_TuneCUETP8M1_13TeV-amcatnlo-pythia8/RunIISpring16MiniAODv2-PUSpring16_80X_mcRun2_asymptotic_2016_miniAODv2_v0-v1/MINIAODSIM',
'/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-madgraphMLM-pythia8/RunIISummer16MiniAODv2-PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6_ext1-v2/MINIAODSIM',
'/WJetsToLNu_TuneCUETP8M1_13TeV-madgraphMLM-pythia8/RunIISummer16MiniAODv2-PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/MINIAODSIM'
#'/WW_TuneCUETP8M1_13TeV-pythia8/RunIISpring16MiniAODv2-PUSpring16_80X_mcRun2_asymptotic_2016_miniAODv2_v0-v1/MINIAODSIM',
#'/WZ_TuneCUETP8M1_13TeV-pythia8/RunIISpring16MiniAODv2-PUSpring16_80X_mcRun2_asymptotic_2016_miniAODv2_v0-v1/MINIAODSIM',
#'/ZZ_TuneCUETP8M1_13TeV-pythia8/RunIISpring16MiniAODv2-PUSpring16_80X_mcRun2_asymptotic_2016_miniAODv2_v0-v1/MINIAODSIM'
                 ]
 for d in range(0,len(datasetnames)):
  config.section_('General')
  config.General.requestName = datasetnames[d]
  config.General.workArea    = datasetnames[d]
  config.section_('JobType')
  config.JobType.pluginName  = 'Analysis'
  config.JobType.psetName    = '/afs/cern.ch/work/f/fromeo/CMSSW_8_0_20_FW/src/BSMFramework/BSM3G_TNT_Maker/python/miniAOD_MCbj.py'
  config.JobType.allowUndistributedCMSSW = True
  config.section_('Data')
  config.Data.inputDataset   = datasetinputs[d]
  config.Data.inputDBS       = 'global'
  config.Data.splitting      = 'FileBased'
  config.Data.totalUnits     = 2500 #With 'FileBased' splitting tells how many files to analyse
  config.Data.unitsPerJob    = 3    #Tells you how many units per job
  #config.Data.outLFNDirBase  = '/store/user/fromeo/'
  config.Data.outLFNDirBase = '/store/group/cmst3/user/fromeo/TTHbb/'
  config.section_('Site')
  config.Site.storageSite    = 'T2_CH_CERN'#T2_CN_Beijing'
  submit(config)
