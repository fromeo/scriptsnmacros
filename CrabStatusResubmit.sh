#!/bin/bash
#list=(TTHbbjpt10_TTHbb TTHbbjpt10_TT TTHbbjpt10_ST TTHbbjpt10_SaT TTHbbjpt10_TTWqq TTHbbjpt10_DY TTHbbjpt10_WJets TTHbbjpt10_WW TTHbbjpt10_WZ TTHbbjpt10_ZZ TTHbbjpt10_SEleB TTHbbjpt10_SEleC TTHbbjpt10_SEleD TTHbbjpt10_SMuB TTHbbjpt10_SMuC TTHbbjpt10_SMuD2)
list=(TTHbbjpt10_TT) #Hnbb2) #TTHbbjpt10_TTWqq3 TTHbbjpt10_TTZqq3) #TTHbbjpt10_TTHbb2 TTHbbjpt10_TTHbb TTHbbjpt10_TT2 TTHbbjpt10_TT TTHbbjpt10_SEleB TTHbbjpt10_SEleC)
rm CrabStatus.txt
pos=0
for d in ${list[@]}; 
do
  crab status --verboseErrors $d/crab_$d >> CrabStatus.txt
  crab status $d/crab_$d > temp.txt
  echo "${list[$pos]}"
  cat temp.txt | grep -A 7 "Jobs status:" | grep 'idle\|unsubmitted\|running\|transferring\|transferred\|failed\|finished'
  #crab resubmit $d/crab_$d
  let pos=pos+1
done
rm temp.txt
