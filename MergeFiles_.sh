#!/bin/bash
analysis=TTHbb_SL #HeavyNeutrino_DR #HeavyNeutrino_SigTopDY #EventCounter
series=TTHbbjpt10_
tasks=(
TT
)
datasets=(
TT_TuneCUETP8M1_13TeV-powheg-pythia8
)
storagepath=/eos/cms/store/cmst3/user/fromeo/TTHbbjpt10/
rm -r $analysis
mkdir $analysis
pos=0
for d in ${tasks[@]}; 
do
  cd $series${tasks[$pos]}/$analysis  
  rm ${tasks[$pos]}\_$analysis\.root
  hadd ${tasks[$pos]}\_$analysis\.root *root
  cd ../..
  mv $series${tasks[$pos]}/$analysis/${tasks[$pos]}\_$analysis\.root $analysis
  #rm -r $series${tasks[$pos]}/$analysis
  let pos=pos+1
done
